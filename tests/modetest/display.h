/**
 * \file display.h
 * \brief Display process
 * \author LaCroix
 *
 * Copyright (c) 2021 LaCroix
 *
 */

/***********************************************************************************************************************
 * THIS SOFTWARE IS PROVIDED BY LACROIX AND CONTRIBUTORS "AS IS" AND ANY EXPRESS, IMPLIED OR STATUTORY
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT
 * PERMITTED BY LAW. IN NO EVENT SHALL LACROIX OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***********************************************************************************************************************/

#ifndef DISPLAY_H
#define DISPLAY_H

/* Configuration files */
#include "benchmark_config.h"

/***************************************************************************
*   Section: Defines
*/

/***************************************************************************
*   Section: Global Variables
*/

/***************************************************************************
*   Section: Global Functions
*/

/*!
 *  @brief          Sample application init.
 *  @details
 *  @param          none
 *  @return         Success or not
*/
uint32_t display_task_init (void);

/*!
 *  @brief          Sample application run.
 *  @details
 *  @param          history_id - history index to save two sequential frames
 *  @return         Success or not
*/
uint32_t display_task_run (uint8_t history_id);

/*!
 *  @brief          Sample application quit.
 *  @details
 *  @param          none
 *  @return         Success or not
*/
uint32_t display_task_quit (void);

#endif /* DISPLAY_H */

/* End of File */
