/*
* DRM based mode setting test program
* Copyright 2008 Tungsten Graphics
*   Jakob Bornecrantz <jakob@tungstengraphics.com>
* Copyright 2008 Intel Corporation
*   Jesse Barnes <jesse.barnes@intel.com>
*
* Permission is hereby granted, free of charge, to any person obtaining a
* copy of this software and associated documentation files (the "Software"),
* to deal in the Software without restriction, including without limitation
* the rights to use, copy, modify, merge, publish, distribute, sublicense,
* and/or sell copies of the Software, and to permit persons to whom the
* Software is furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
* IN THE SOFTWARE.
*/

/*
* This fairly simple test program dumps output in a similar format to the
* "xrandr" tool everyone knows & loves.  It's necessarily slightly different
* since the kernel separates outputs into encoder and connector structures,
* each with their own unique ID.  The program also allows test testing of the
* memory management and mode setting APIs by allowing the user to specify a
* connector and mode to use for mode setting.  If all works as expected, a
* blue background should be painted on the monitor attached to the specified
* connector after the selected mode is set.
*
* TODO: use cairo to write the mode info on the selected output once
*       the mode has been programmed, along with possible test patterns.
*/

#include <assert.h>
#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include <errno.h>
#include <poll.h>
#include <sys/time.h>
#if HAVE_SYS_SELECT_H
#include <sys/select.h>
#endif
#include <math.h>

#include "xf86drm.h"
#include "xf86drmMode.h"
#include "drm_fourcc.h"

#include "util/common.h"
#include "util/format.h"
#include "util/kms.h"
#include "util/pattern.h"

#include "buffers.h"
#include "cursor.h"

#include <gst/gst.h>
#include <gst/video/video.h>
#include <gst/app/gstappsink.h>
#include <stdio.h>
#include <unistd.h>

static enum util_fill_pattern primary_fill = UTIL_PATTERN_SMPTE;
static enum util_fill_pattern secondary_fill = UTIL_PATTERN_TILES;

struct crtc {
    drmModeCrtc *crtc;
    drmModeObjectProperties *props;
    drmModePropertyRes **props_info;
    drmModeModeInfo *mode;
};

struct encoder {
    drmModeEncoder *encoder;
};

struct connector {
    drmModeConnector *connector;
    drmModeObjectProperties *props;
    drmModePropertyRes **props_info;
    char *name;
};

struct fb {
    drmModeFB *fb;
};

struct plane {
    drmModePlane *plane;
    drmModeObjectProperties *props;
    drmModePropertyRes **props_info;
};

struct resources {
    struct crtc *crtcs;
    int count_crtcs;
    struct encoder *encoders;
    int count_encoders;
    struct connector *connectors;
    int count_connectors;
    struct fb *fbs;
    int count_fbs;
    struct plane *planes;
    uint32_t count_planes;
};

struct device {
    int fd;

    struct resources *resources;

    struct {
        unsigned int width;
        unsigned int height;

        unsigned int fb_id;
        struct bo *bo;
        struct bo *cursor_bo;
    } mode;

    int use_atomic;
    drmModeAtomicReq *req;
};

struct plane_arg {
    uint32_t plane_id;  /* the id of plane to use */
    uint32_t crtc_id;  /* the id of CRTC to bind to */
    bool has_position;
    int32_t x, y;
    uint32_t w, h;
    double scale;
    unsigned int fb_id;
    unsigned int old_fb_id;
    struct bo *bo;
    struct bo *old_bo;
    char format_str[5]; /* need to leave room for terminating \0 */
    unsigned int fourcc;
};

struct position {
    uint32_t plane_id;
    int32_t x;
    int32_t y;
};

struct position planes_stored_data[] = {
    { 36, 0, 0 },
    { 38, 0, 384 },
    { 40, 512, 0 },
    { 42, 512, 384 },
};

char *fr_fl_sr_sl[4] = { 
    "7e5c2b9c8b64d07ad2c77fa2d36d836fb015258fdd71e8eb99cf6078e24a82ba",
    "a2b4ad385aa989186b52ed4ced0191a7c101ce6517478958f40667228d974910",
    "791d4f80005d5a083291091936dcd7af8611c3202618e9c10e3b860c4f61b03b",
    "51919a602e7b6d6993663bc331cb1a9ceb162f2dabb58a48f4908bb07f73495b"
};

/*
*   Section: Local variables
*/
struct device dev;

#define num_planes 4
#define num_streams 4

char *device = NULL;
char *module = NULL;
unsigned int plane_count = num_planes;
struct plane_arg *plane_args = NULL;
int counter = 0;
uint16_t * buffer_plane_0;
char * last_stream_id = NULL;
char stream_id_to_plane[num_streams][65] = { "" };

/***********************************************************************************************************************
*   Section: Defines
*/
#define DISPLAY_WIDTH           (512)
#define DISPLAY_HEIGHT          (384)

/** Metrics information **/
/* Background color */
#define METRIC_COLOR_Y           (131)
#define METRIC_COLOR_U           (147)
#define METRIC_COLOR_V           (119)
#define METRIC_RECT_CORNER       (20)


/***************************************************************************
*   Function: display_rectangle
*
*   Description:
*       Display rectangle
*
*   Parameters:
*       buffer - output buffer
*       start_x - start position x
*       start_y - start position y
*       width - width of the rectangle
*       height - height of the rectangle
*       corner - radius of the circle
*
*   Return:
*       Success or not
*/
static uint32_t display_rectangle(uint16_t *buffer, uint32_t start_x, uint32_t start_y, uint32_t width, uint32_t height,
        uint32_t corner)
{
    uint8_t y;
    uint8_t uv;
    uint32_t i;
    uint32_t j;

    for (i = start_y; i < (start_y + height); ++i)
    {
        for (j = start_x; j < (start_x + width); ++j)
        {
            y = METRIC_COLOR_Y;
            uv = ((j % 2) == 0) ? METRIC_COLOR_U : METRIC_COLOR_V;

            buffer[(i * DISPLAY_WIDTH) + j] = (y << 0) + (uv << 8);
        }
    }

    return 0;
}


static void free_resources(struct resources *res)
{
    int i;

    if (!res)
        return;

#define free_resource(_res, type, Type)					\
    do {									\
        if (!(_res)->type##s)						\
            break;							\
        for (i = 0; i < (int)(_res)->count_##type##s; ++i) {	\
            if (!(_res)->type##s[i].type)				\
                break;						\
            drmModeFree##Type((_res)->type##s[i].type);		\
        }								\
        free((_res)->type##s);						\
    } while (0)

#define free_properties(_res, type)					\
    do {									\
        for (i = 0; i < (int)(_res)->count_##type##s; ++i) {	\
            unsigned int j;										\
            for (j = 0; j < res->type##s[i].props->count_props; ++j)\
                drmModeFreeProperty(res->type##s[i].props_info[j]);\
            free(res->type##s[i].props_info);			\
            drmModeFreeObjectProperties(res->type##s[i].props);	\
        }								\
    } while (0)

    free_properties(res, plane);
    free_resource(res, plane, Plane);

    free_properties(res, connector);
    free_properties(res, crtc);

    for (i = 0; i < res->count_connectors; i++)
        free(res->connectors[i].name);

    free_resource(res, fb, FB);
    free_resource(res, connector, Connector);
    free_resource(res, encoder, Encoder);
    free_resource(res, crtc, Crtc);

    free(res);
}

static struct resources *get_resources(struct device *dev)
{
    drmModeRes *_res;
    drmModePlaneRes *plane_res;
    struct resources *res;
    int i;

    res = calloc(1, sizeof(*res));
    if (res == 0)
        return NULL;

    drmSetClientCap(dev->fd, DRM_CLIENT_CAP_UNIVERSAL_PLANES, 1);

    _res = drmModeGetResources(dev->fd);
    if (!_res) {
        fprintf(stderr, "drmModeGetResources failed: %s\n",
            strerror(errno));
        free(res);
        return NULL;
    }

    res->count_crtcs = _res->count_crtcs;
    res->count_encoders = _res->count_encoders;
    res->count_connectors = _res->count_connectors;
    res->count_fbs = _res->count_fbs;

    res->crtcs = calloc(res->count_crtcs, sizeof(*res->crtcs));
    res->encoders = calloc(res->count_encoders, sizeof(*res->encoders));
    res->connectors = calloc(res->count_connectors, sizeof(*res->connectors));
    res->fbs = calloc(res->count_fbs, sizeof(*res->fbs));

    if (!res->crtcs || !res->encoders || !res->connectors || !res->fbs) {
        drmModeFreeResources(_res);
        goto error;
    }

#define get_resource(_res, __res, type, Type)					\
    do {									\
        for (i = 0; i < (int)(_res)->count_##type##s; ++i) {	\
            uint32_t type##id = (__res)->type##s[i];			\
            (_res)->type##s[i].type =							\
                drmModeGet##Type(dev->fd, type##id);			\
            if (!(_res)->type##s[i].type)						\
                fprintf(stderr, "could not get %s %i: %s\n",	\
                    #type, type##id,							\
                    strerror(errno));			\
        }								\
    } while (0)

    get_resource(res, _res, crtc, Crtc);
    get_resource(res, _res, encoder, Encoder);
    get_resource(res, _res, connector, Connector);
    get_resource(res, _res, fb, FB);

    drmModeFreeResources(_res);

    /* Set the name of all connectors based on the type name and the per-type ID. */
    for (i = 0; i < res->count_connectors; i++) {
        struct connector *connector = &res->connectors[i];
        drmModeConnector *conn = connector->connector;
        int num;

        num = asprintf(&connector->name, "%s-%u",
            util_lookup_connector_type_name(conn->connector_type),
            conn->connector_type_id);
        if (num < 0)
            goto error;
    }

#define get_properties(_res, type, Type)					\
    do {									\
        for (i = 0; i < (int)(_res)->count_##type##s; ++i) {	\
            struct type *obj = &res->type##s[i];			\
            unsigned int j;						\
            obj->props =						\
                drmModeObjectGetProperties(dev->fd, obj->type->type##_id, \
                            DRM_MODE_OBJECT_##Type); \
            if (!obj->props) {					\
                fprintf(stderr,					\
                    "could not get %s %i properties: %s\n", \
                    #type, obj->type->type##_id,		\
                    strerror(errno));			\
                continue;					\
            }							\
            obj->props_info = calloc(obj->props->count_props,	\
                        sizeof(*obj->props_info));	\
            if (!obj->props_info)					\
                continue;					\
            for (j = 0; j < obj->props->count_props; ++j)		\
                obj->props_info[j] =				\
                    drmModeGetProperty(dev->fd, obj->props->props[j]); \
        }								\
    } while (0)

    get_properties(res, crtc, CRTC);
    get_properties(res, connector, CONNECTOR);

    for (i = 0; i < res->count_crtcs; ++i)
        res->crtcs[i].mode = &res->crtcs[i].crtc->mode;

    plane_res = drmModeGetPlaneResources(dev->fd);
    if (!plane_res) {
        fprintf(stderr, "drmModeGetPlaneResources failed: %s\n",
            strerror(errno));
        return res;
    }

    res->count_planes = plane_res->count_planes;

    res->planes = calloc(res->count_planes, sizeof(*res->planes));
    if (!res->planes) {
        drmModeFreePlaneResources(plane_res);
        goto error;
    }

    get_resource(res, plane_res, plane, Plane);
    drmModeFreePlaneResources(plane_res);
    get_properties(res, plane, PLANE);

    return res;

error:
    free_resources(res);
    return NULL;
}

static struct crtc *get_crtc_by_id(struct device *dev, uint32_t id)
{
    int i;

    for (i = 0; i < dev->resources->count_crtcs; ++i) {
        drmModeCrtc *crtc = dev->resources->crtcs[i].crtc;
        if (crtc && crtc->crtc_id == id)
            return &dev->resources->crtcs[i];
    }

    return NULL;
}

static uint32_t get_crtc_mask(struct device *dev, struct crtc *crtc)
{
    unsigned int i;

    for (i = 0; i < (unsigned int)dev->resources->count_crtcs; i++) {
        if (crtc->crtc->crtc_id == dev->resources->crtcs[i].crtc->crtc_id)
            return 1 << i;
    }
    /* Unreachable: crtc->crtc is one of resources->crtcs[] */
    /* Don't return zero or static analysers will complain */
    abort();
    return 0;
}

static bool format_support(const drmModePlanePtr ovr, uint32_t fmt)
{
    unsigned int i;

    for (i = 0; i < ovr->count_formats; ++i) {
        if (ovr->formats[i] == fmt)
            return true;
    }

    return false;
}

static int
bo_fb_create(int fd, unsigned int fourcc, const uint32_t w, const uint32_t h,
            enum util_fill_pattern pat, struct bo **out_bo, unsigned int *out_fb_id)
{
    uint32_t handles[4] = {0}, pitches[4] = {0}, offsets[4] = {0};
    struct bo *bo;
    unsigned int fb_id;

    bo = bo_create(fd, fourcc, w, h, handles, pitches, offsets, pat);

    if (bo == NULL)
        return -1;

    if (drmModeAddFB2(fd, w, h, fourcc, handles, pitches, offsets, &fb_id, 0)) {
        fprintf(stderr, "failed to add fb (%ux%u): %s\n", w, h, strerror(errno));
        bo_destroy(bo);
        return -1;
    }
    *out_bo = bo;
    *out_fb_id = fb_id;
    return 0;
}

static int set_plane(struct device *dev, struct plane_arg *p)
{
    drmModePlane *ovr;
    uint32_t plane_id;
    int crtc_x, crtc_y, crtc_w, crtc_h;
    struct crtc *crtc = NULL;
    unsigned int i, crtc_mask;

    /* Find an unused plane which can be connected to our CRTC. Find the
    * CRTC index first, then iterate over available planes.
    */
    crtc = get_crtc_by_id(dev, p->crtc_id);
    if (!crtc) {
        fprintf(stderr, "CRTC %u not found\n", p->crtc_id);
        return -1;
    }
    crtc_mask = get_crtc_mask(dev, crtc);
    plane_id = p->plane_id;

    for (i = 0; i < dev->resources->count_planes; i++) {
        ovr = dev->resources->planes[i].plane;
        if (!ovr)
            continue;

        if (plane_id && plane_id != ovr->plane_id)
            continue;

        if (!format_support(ovr, p->fourcc))
            continue;

        if ((ovr->possible_crtcs & crtc_mask) &&
            (ovr->crtc_id == 0 || ovr->crtc_id == p->crtc_id)) {
            plane_id = ovr->plane_id;
            break;
        }
    }

    if (i == dev->resources->count_planes) {
        fprintf(stderr, "no unused plane available for CRTC %u\n",
            p->crtc_id);
        return -1;
    }

    fprintf(stderr, "testing %dx%d@%s overlay plane %u\n",
        p->w, p->h, p->format_str, plane_id);

    /* just use single plane format for now.. */
    if (bo_fb_create(dev->fd, p->fourcc, p->w, p->h,
                    secondary_fill, &p->bo, &p->fb_id))
        return -1;

    crtc_w = p->w * p->scale;
    crtc_h = p->h * p->scale;
    if (!p->has_position) {
        /* Default to the middle of the screen */
        crtc_x = (crtc->mode->hdisplay - crtc_w) / 2;
        crtc_y = (crtc->mode->vdisplay - crtc_h) / 2;
    } else {
        crtc_x = p->x;
        crtc_y = p->y;
    }

    /* note src coords (last 4 args) are in Q16 format */
    if (drmModeSetPlane(dev->fd, plane_id, p->crtc_id, p->fb_id,
                0, crtc_x, crtc_y, crtc_w, crtc_h,
                0, 0, p->w << 16, p->h << 16)) {
        fprintf(stderr, "failed to enable plane: %s\n",
            strerror(errno));
        return -1;
    }

    ovr->crtc_id = p->crtc_id;

    return 0;
}

static void set_planes(struct device *dev, struct plane_arg *p, unsigned int count)
{
    unsigned int i;

    /* set up planes/overlays */
    for (i = 0; i < count; i++)
        if (set_plane(dev, &p[i]))
            return;
}

static void clear_planes(struct device *dev, struct plane_arg *p, unsigned int count)
{
    unsigned int i;

    for (i = 0; i < count; i++) {
        if (p[i].fb_id)
            drmModeRmFB(dev->fd, p[i].fb_id);
        if (p[i].bo)
            unmap_bo(p[i].bo);
            bo_destroy(p[i].bo);
    }
}


/***********************************************************************************************************************
*   Section: Structures
*/

int init_display()
{
    unsigned int i;

    memset(&dev, 0, sizeof dev);

    for (i = 0; i < plane_count; ++i)
    {
        plane_args = realloc (plane_args, (plane_count + 1) * sizeof(*plane_args));
        if (plane_args == NULL)
        {
            fprintf (stderr, "memory allocation failed\n");
            return 1;
        }
        memset (&plane_args[plane_count], 0, sizeof(*plane_args));

        int height = DISPLAY_HEIGHT;
        int width = DISPLAY_WIDTH;

        plane_args[i].plane_id      = planes_stored_data[i].plane_id;
        plane_args[i].crtc_id       = 44;
        plane_args[i].w             = width;
        plane_args[i].h             = height;
        plane_args[i].x             = planes_stored_data[i].x;
        plane_args[i].y             = planes_stored_data[i].y;
        plane_args[i].has_position  = true;
        plane_args[i].scale         = 1.0;
        strcpy (plane_args[i].format_str, "YUYV");
        plane_args[i].fourcc        = util_format_fourcc (plane_args[i].format_str);
    }

    dev.fd = util_open(device, "rcar-du");
    if (dev.fd < 0)
        return -1;

    dev.resources = get_resources(&dev);

    if (!dev.resources) {
        drmClose(dev.fd);
        return 1;
    }
}

int deinit_display()
{
    clear_planes(&dev, plane_args, plane_count);

    free_resources(dev.resources);
    drmClose(dev.fd);

    return 0;
}

int display_on_plane(uint16_t* data, int plane_id)
{  
    buffer_plane_0 = (uint16_t*) get_buffer (plane_args[plane_id % 4].bo);

    // memmove or memcpy?
    if (plane_id >= 4)
    {
        buffer_plane_0 += DISPLAY_HEIGHT*DISPLAY_WIDTH;
    }

    if (true || plane_id < 4)
    {
        memmove(buffer_plane_0, data, sizeof(uint16_t)*DISPLAY_HEIGHT*DISPLAY_WIDTH);
    }

}

void print_planes_dict()
{
    for (int i=0; i < plane_count; i++)
    {
        printf("[%d] - %s \n", i, stream_id_to_plane[i]);
    }

    printf("\n");
}

int find_or_allocate_plane(char* stream_id, bool new)
{
    if (!new) 
    {
        for (int i=0; i < num_streams; i++)
        {
            if (strncmp(stream_id_to_plane[i], stream_id, 64) == 0)
            {
                // printf("Found during cmp \n");
                // print_planes_dict();
                return i;
            }
        }

        for (int i=0; i < num_streams; i++)
        {
            if (stream_id_to_plane[i][0] == '\000')
            {
                // printf("Before CP \n");
                // print_planes_dict();
                memcpy(stream_id_to_plane[i], stream_id, 64);
                stream_id_to_plane[i][64] = '\0';

                // strcpy(stream_id_to_plane[i], stream_id);
                // printf("After CP \n");
                print_planes_dict();
                return i;
            }
        }
    }
    else
    {
        for (int i=0; i < num_streams; i++)
        {
            if (strncmp(fr_fl_sr_sl[i], stream_id, 64) == 0)
            {
                return i;
            }
        }
    }
    

    return -1;
}

static GstFlowReturn appsink_cb(GstAppSink * sink, gpointer udata)
{
    counter++;
    // // printf("Test CB %d\n", counter);

    GstSample *sample;  // The new sample available in the appsink
    GstBuffer *buffer;  // The buffer stored in that sample
    GstMapInfo map;
    GstPad *pad = (GstPad *)udata;

    // // Get the sample from the appsink
    sample = gst_app_sink_pull_sample(sink);        
    // // If no sample was available (premature  signal) then return, we will get it on the next pass
    if (sample == NULL)
        return GST_FLOW_OK;

    buffer = gst_sample_get_buffer(sample);    // Extract the buffer from the sample
    
    if (gst_buffer_map(buffer, &map, GST_MAP_READ))
    {
        uint16_t * data = (uint16_t *)map.data;
        
        gchar *stream_id = gst_pad_get_stream_id (pad);

        if (stream_id != NULL) 
        {
            int plane_id = find_or_allocate_plane(stream_id, true);
            // printf("Got stream id [%d] %s \n", plane_id, stream_id);
            
            display_on_plane(data, plane_id);
        }

        gst_buffer_unmap(buffer, &map);
    }
    gst_sample_unref(sample);

    return GST_FLOW_OK;
}
/**
* @brief Handle ending messages from the pipeline (EOS, errors, etc...)
* 
* @param bus  -  GstBus*
*        The GstBus to handle.
*
* @param msg  -  GstMessage*
*        The GstMessage to handle.
*/
static gboolean my_bus_callback (GstBus * bus, GstMessage * message, gpointer udata)
{
GstElement *pipeline = (GstElement *)udata;

switch (GST_MESSAGE_TYPE (message)) {
    case GST_MESSAGE_ERROR:{
    GError *err;
    gchar *debug;

    gst_message_parse_error (message, &err, &debug);
    g_print ("Error: %s\n", err->message);
    g_error_free (err);
    g_free (debug);

    //   pipe->stop();
    break;
    }
    case GST_MESSAGE_EOS:{
    /* end-of-stream */
    g_print ("End of stream\n");
    printf("Hey, I got EOS on %s \n", message->src->name);
    gst_element_seek_simple(pipeline, GST_FORMAT_TIME, (GstSeekFlags) (GST_SEEK_FLAG_FLUSH | GST_SEEK_FLAG_KEY_UNIT) , 0);
    break;
    }
    case GST_EVENT_SEGMENT_DONE: {
    /* end-of-stream */
    g_print ("Seg done of stream\n");
    printf("Hey, I got EOS on %s \n", message->src->name);
    gst_element_seek_simple(pipeline, GST_FORMAT_TIME, (GstSeekFlags) (GST_SEEK_FLAG_FLUSH | GST_SEEK_FLAG_KEY_UNIT) , 0);
    break;
    }
    default:
    /* unhandled message */
    break;
}

/* we want to be notified again the next time there is a message
* on the bus, so returning TRUE (FALSE means we want to stop watching
* for messages on the bus and our callback should not be called again)
*/
return TRUE;
}

int main(int argc, char **argv)
{
    init_display();
    set_planes(&dev, plane_args, plane_count);
    
    // Prepare pipeline components
    GstBus *bus;
    GMainLoop *loop;
    gst_init(&argc, &argv);  // Initialize Gstreamer
    loop = g_main_loop_new (NULL, FALSE);


    GstElement *pipeline = gst_parse_launch("filesrc location=/home/root/videos/20190401121727_camera_frontleft.mp4.raw ! queue leaky=no max-size-buffers=1 max-size-bytes=0 max-size-time=0 ! rawvideoparse use-sink-caps=false width=512 height=384 format=yuy2 framerate=24/1 !  queue leaky=no max-size-buffers=1 max-size-bytes=0 max-size-time=0 ! fun.sink_0 filesrc location=/home/root/videos/20190401121727_camera_frontright.mp4.raw ! queue leaky=no max-size-buffers=1 max-size-bytes=0 max-size-time=0 ! rawvideoparse use-sink-caps=false width=512 height=384 format=yuy2 framerate=24/1 ! queue leaky=no max-size-buffers=1 max-size-bytes=0 max-size-time=0 ! fun.sink_1 filesrc location=/home/root/videos/20190401121727_camera_sideright.mp4.raw ! queue leaky=no max-size-buffers=1 max-size-bytes=0 max-size-time=0 ! rawvideoparse use-sink-caps=false width=512 height=384 format=yuy2 framerate=24/1 ! queue leaky=no max-size-buffers=1 max-size-bytes=0 max-size-time=0 ! fun.sink_2 filesrc location=/home/root/videos/20190401121727_camera_sideleft.mp4.raw ! queue leaky=no max-size-buffers=1 max-size-bytes=0 max-size-time=0 ! rawvideoparse use-sink-caps=false width=512 height=384 format=yuy2 framerate=24/1 ! queue leaky=no max-size-buffers=1 max-size-bytes=0 max-size-time=0 ! fun.sink_3 funnel name=fun ! queue leaky=no max-size-buffers=3 max-size-bytes=0 max-size-time=0 ! hailonet hef-path=/home/root/yolov5m_a2d2_384_512.hef  debug=False is-active=true qos=false ! queue leaky=no max-size-buffers=3 max-size-bytes=0 max-size-time=0 ! hailofilter2 function-name=yolov5_adas so-path=/usr/lib/hailo-post-processes/libnew_yolo_post.so qos=false ! queue leaky=no max-size-buffers=3 max-size-bytes=0 max-size-time=0 ! hailooverlay ! queue leaky=no max-size-buffers=3 max-size-bytes=0 max-size-time=0 ! appsink sync=true qos=false emit-signals=true name=test_app_sink", NULL);

    
    bus = gst_element_get_bus(pipeline);
    gst_bus_add_watch (bus, my_bus_callback, pipeline);
    
    GstElement *app_sink_element = gst_bin_get_by_name(GST_BIN(pipeline), "test_app_sink");
    GstPad * pad = gst_element_get_static_pad(app_sink_element, "sink");

    g_signal_connect(app_sink_element, "new-sample", G_CALLBACK(appsink_cb), pad);

    gst_element_set_state(pipeline, GST_STATE_PLAYING);
    g_main_loop_run(loop);

    // Free resources
    gst_element_set_state(pipeline, GST_STATE_NULL);

    gst_deinit();
    gst_object_unref(pad);
    gst_object_unref(pipeline);
    gst_object_unref(bus);
    g_main_loop_unref (loop);

    deinit_display();

    return 0;
}
