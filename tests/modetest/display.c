/**
 * \file display.c
 * \brief Display process
 * \author LaCroix
 *
 * Copyright (c) 2021 LaCroix
 *
 */

/***********************************************************************************************************************
 * THIS SOFTWARE IS PROVIDED BY LACROIX AND CONTRIBUTORS "AS IS" AND ANY EXPRESS, IMPLIED OR STATUTORY
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT
 * PERMITTED BY LAW. IN NO EVENT SHALL LACROIX OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***********************************************************************************************************************/

/***********************************************************************************************************************
*   Section: Includes
*/

#define _GNU_SOURCE

/* Header include */
#include "display.h"

/* OS includes */
#include <stdio.h>
#include <assert.h>
#include <ctype.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include <errno.h>
#include <poll.h>
#include <sys/time.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "xf86drm.h"
#include "xf86drmMode.h"
#include "drm_fourcc.h"
#include "drm/rcar_du_drm.h"

/* Project includes */
#include "util/common.h"
#include "util/format.h"
#include "util/kms.h"
#include "buffers.h"
#include "debug.h"
#include "text.h"
#include "logo.h"

/* IMP Driver, ATMLIB and OSAL library */
#include "rcar-xos/osal/r_osal.h"

/***********************************************************************************************************************
*   Section: Defines
*/

/* Sub image size */
#define SUB_IMAGE_FACTOR        (4)
#define SUB_IMAGE_WIDTH         (300)
#define SUB_IMAGE_HEIGHT        (1080 / SUB_IMAGE_FACTOR)

/* Screen size */
#define DISPLAY_WIDTH           (1920)
#define DISPLAY_HEIGHT          (1080)

/* RGB to YUV */
#define MAKE_YUV_601_Y(r, g, b) \
    ((( 66 * (r) + 129 * (g) +  25 * (b) + 128) >> 8) + 16)
#define MAKE_YUV_601_U(r, g, b) \
    (((-38 * (r) -  74 * (g) + 112 * (b) + 128) >> 8) + 128)
#define MAKE_YUV_601_V(r, g, b) \
    (((112 * (r) -  94 * (g) -  18 * (b) + 128) >> 8) + 128)
#define RGB_NB_PLAN             (3)

/* DOF management */
#define HORIZONTAL_MASK          (0xFF800000)        /* Horizontal mask used to extract horizontal motion component */
#define VERTICAL_MASK            (0x003FE000)        /* Vertical mask used to extract vertical motion component */
#define QUALITY_MASK             (0x00000003)        /* Quality mask used to extract quality of the computed flow */


/** Images **/
#define SUB_IMAGE_ISP_Y          (40)
#define SUB_IMAGE_ISP_0_X        (300)
#define SUB_IMAGE_ISP_1_X        (640)
#define SUB_IMAGE_ISP_2_X        (980)
#define SUB_IMAGE_ISP_3_X        (1320)
#define SUB_IMAGE_DOF_Y          (420)
#define SUB_IMAGE_DOF_0_X        (300)
#define SUB_IMAGE_DOF_1_X        (640)
#define SUB_IMAGE_DOF_2_X        (980)
#define SUB_IMAGE_DOF_3_X        (1320)


/** Metrics information **/
/* Background color */
#define METRIC_COLOR_Y           (131)
#define METRIC_COLOR_U           (147)
#define METRIC_COLOR_V           (119)
#define METRIC_RECT_CORNER       (20)

/* Rectangle size */
#define METRIC_RECT_WIDTH        (1320)
#define METRIC_RECT_HEIGHT       (240)
#define METRIC_RECT_START_X      ((DISPLAY_WIDTH - METRIC_RECT_WIDTH) / 2)
#define METRIC_RECT_START_Y      (800)

/* Text position */
#define METRIC_SPACE_WIDTH        (30)
#define METRIC_VALUE_OFFSET      (((2 * METRIC_SPACE_WIDTH) + TEXT_METRIC_BANDWIDTH_WIDTH) + METRIC_RECT_START_X)

#define METRIC_FPS_X             (METRIC_RECT_START_X + METRIC_SPACE_WIDTH)
#define METRIC_FPS_Y             (METRIC_RECT_START_Y + ((METRIC_RECT_HEIGHT * 1) / 4) - (TEXT_DIGIT_HEIGHT / 2))
#define METRIC_BANDWIDTH_X       (METRIC_RECT_START_X + METRIC_SPACE_WIDTH)
#define METRIC_BANDWIDTH_Y       (METRIC_RECT_START_Y + ((METRIC_RECT_HEIGHT * 2) / 4) - (TEXT_DIGIT_HEIGHT / 2))
#define METRIC_LOAD_RATE_X       (METRIC_RECT_START_X + METRIC_SPACE_WIDTH)
#define METRIC_LOAD_RATE_Y       (METRIC_RECT_START_Y + ((METRIC_RECT_HEIGHT * 3) / 4) - (TEXT_DIGIT_HEIGHT / 2))

#define METRIC_FPS_VALUE_X       (METRIC_VALUE_OFFSET)

#define METRIC_ISP_X             ((0 * (METRIC_RECT_WIDTH - (((2 * METRIC_SPACE_WIDTH) + TEXT_METRIC_BANDWIDTH_WIDTH) + TEXT_METRIC_ISP_WIDTH)) / 6) \
        + METRIC_VALUE_OFFSET + TEXT_METRIC_ISP_WIDTH)
#define METRIC_IMR_X             ((1 * (METRIC_RECT_WIDTH - (((2 * METRIC_SPACE_WIDTH) + TEXT_METRIC_BANDWIDTH_WIDTH) + TEXT_METRIC_ISP_WIDTH)) / 6) \
        + METRIC_VALUE_OFFSET + TEXT_METRIC_ISP_WIDTH)
#define METRIC_HOG_X             ((2 * (METRIC_RECT_WIDTH - (((2 * METRIC_SPACE_WIDTH) + TEXT_METRIC_BANDWIDTH_WIDTH) + TEXT_METRIC_ISP_WIDTH)) / 6) \
        + METRIC_VALUE_OFFSET + TEXT_METRIC_ISP_WIDTH)
#define METRIC_PSC_X             ((3 * (METRIC_RECT_WIDTH - (((2 * METRIC_SPACE_WIDTH) + TEXT_METRIC_BANDWIDTH_WIDTH) + TEXT_METRIC_ISP_WIDTH)) / 6) \
        + METRIC_VALUE_OFFSET + TEXT_METRIC_ISP_WIDTH)
#define METRIC_DOF_X             ((4 * (METRIC_RECT_WIDTH - (((2 * METRIC_SPACE_WIDTH) + TEXT_METRIC_BANDWIDTH_WIDTH) + TEXT_METRIC_ISP_WIDTH)) / 6) \
        + METRIC_VALUE_OFFSET + TEXT_METRIC_ISP_WIDTH)
#define METRIC_CPU_X             ((5 * (METRIC_RECT_WIDTH - (((2 * METRIC_SPACE_WIDTH) + TEXT_METRIC_BANDWIDTH_WIDTH) + TEXT_METRIC_ISP_WIDTH)) / 6) \
        + METRIC_VALUE_OFFSET + TEXT_METRIC_ISP_WIDTH)

#define METRIC_READ_X            (METRIC_VALUE_OFFSET)
#define METRIC_WRITE_X           (METRIC_READ_X + (TEXT_METRIC_READ_WIDTH + METRIC_SPACE_WIDTH))


/** Logo **/
#define LOGO_LCX_X               ((METRIC_RECT_START_X + METRIC_RECT_WIDTH) + ((METRIC_RECT_START_X - LOGO_WIDTH) / 2))
#define LOGO_LCX_Y               ((DISPLAY_HEIGHT * 1) / 3 - LOGO_LCX_HEIGHT_CENTER)
#define LOGO_REN_X               ((METRIC_RECT_START_X + METRIC_RECT_WIDTH) + ((METRIC_RECT_START_X - LOGO_WIDTH) / 2))
#define LOGO_REN_Y               ((DISPLAY_HEIGHT * 2) / 3 - LOGO_REN_HEIGHT_CENTER)


/** Pathes **/
#define DATA_OUT                 BENCHMARK_OUTPUT_FOLDER"display_output"
#define FILENAME_LENGTH          (512)


/** Titles **/
#define TITLE_RECT_WIDTH         (200)
#define TITLE_RECT_HEIGHT        (SUB_IMAGE_HEIGHT)
#define TITLE_RECT_START_X       (50)
#define TITLE_RECT_START_ISP_Y   (40)
#define TITLE_RECT_START_DOF_Y   (420)
#define TITLE_RECT_CORNER        (20)


/** Sub titles */
#define STITLE_RECT_WIDTH        (SUB_IMAGE_WIDTH)
#define STITLE_RECT_HEIGHT       (40)
#define STITLE_RECT_ISP_Y        (SUB_IMAGE_ISP_Y + SUB_IMAGE_HEIGHT + 10)
#define STITLE_RECT_ISP_0_X      (SUB_IMAGE_ISP_0_X)
#define STITLE_RECT_ISP_1_X      (SUB_IMAGE_ISP_1_X)
#define STITLE_RECT_ISP_2_X      (SUB_IMAGE_ISP_2_X)
#define STITLE_RECT_ISP_3_X      (SUB_IMAGE_ISP_3_X)
#define STITLE_RECT_DOF_Y        (SUB_IMAGE_DOF_Y + SUB_IMAGE_HEIGHT + 10)
#define STITLE_RECT_DOF_0_X      (SUB_IMAGE_DOF_0_X)
#define STITLE_RECT_DOF_1_X      (SUB_IMAGE_DOF_1_X)
#define STITLE_RECT_DOF_2_X      (SUB_IMAGE_DOF_2_X)
#define STITLE_RECT_DOF_3_X      (SUB_IMAGE_DOF_3_X)
#define STITLE_RECT_CORNER       (5)


/***********************************************************************************************************************
*   Section: Structures
*/

struct crtc
{
    drmModeCrtc *crtc;
    drmModeObjectProperties *props;
    drmModePropertyRes **props_info;
    drmModeModeInfo *mode;
};

struct encoder
{
    drmModeEncoder *encoder;
};

struct connector
{
    drmModeConnector *connector;
    drmModeObjectProperties *props;
    drmModePropertyRes **props_info;
    char *name;
};

struct fb
{
    drmModeFB *fb;
};

struct plane
{
    drmModePlane *plane;
    drmModeObjectProperties *props;
    drmModePropertyRes **props_info;
};

struct resources
{
    struct crtc *crtcs;
    int count_crtcs;
    struct encoder *encoders;
    int count_encoders;
    struct connector *connectors;
    int count_connectors;
    struct fb *fbs;
    int count_fbs;
    struct plane *planes;
    uint32_t count_planes;
};

struct device
{
    int fd;
    struct resources *resources;
    struct
    {
        unsigned int width;
        unsigned int height;
        unsigned int fb_id;
        struct bo *bo;
        struct bo *cursor_bo;
    } mode;

    int use_atomic;
    drmModeAtomicReq *req;
};

struct pipe_arg {
    const char **cons;
    uint32_t *con_ids;
    unsigned int num_cons;
    uint32_t crtc_id;
    char mode_str[64];
    char format_str[5];
    float vrefresh;
    unsigned int fourcc;
    drmModeModeInfo *mode;
    struct crtc *crtc;
    unsigned int fb_id[2], current_fb_id;
    struct timeval start;

    int swap_count;
};

struct plane_arg {
    uint32_t plane_id;  /* the id of plane to use */
    uint32_t crtc_id;  /* the id of CRTC to bind to */
    bool has_position;
    int32_t x, y;
    uint32_t w, h;
    double scale;
    unsigned int fb_id;
    unsigned int old_fb_id;
    struct bo *bo;
    struct bo *old_bo;
    char format_str[5]; /* need to leave room for terminating \0 */
    unsigned int fourcc;
};

/***********************************************************************************************************************
*   Section: Local Functions Prototypes
*/

/***********************************************************************************************************************
*   Section: Local variables
*/

enum util_fill_pattern secondary_fill = UTIL_PATTERN_SMPTE;
struct device dev;
struct plane_arg *plane_args = NULL;
uint32_t plane_count;

/***********************************************************************************************************************
*   Section: Public variables
*/

extern benchmark_frame_resolution_t frame_resolution;
extern st_kpi kpi_results;

/***********************************************************************************************************************
*   Section: Global Functions
*/

static struct resources *get_resources(struct device *dev);
static void free_resources(struct resources *res);
static void set_planes(struct device *dev, struct plane_arg *p, unsigned int count);
static void clear_planes(struct device *dev, struct plane_arg *p, unsigned int count);
static uint32_t image_u8_ppm_read(const char *fileName, uint8_t *image);
static void display_overlay_hsv(uint16_t *image, uint32_t *pout_flow_buffer_l2, uint32_t *pout_flow_buffer_l1,
        uint32_t x, uint32_t y, uint8_t *r, uint8_t *g, uint8_t *b, float avg);
static void display_hsv_to_rgb(float *rgb_red, float *rgb_green, float *rgb_blue, float hsv_hue, float hsv_sat,
        float hsv_val);
static float display_compute_vector_dof(uint32_t *pout_flow_buffer_l2, uint32_t *pout_flow_buffer_l1, uint32_t x,
        uint32_t y);
static uint32_t display_rectangle(uint16_t *buffer, uint32_t start_x, uint32_t start_y, uint32_t width, uint32_t height,
        uint32_t corner);
static uint32_t display_number(float num, uint8_t select_fps, uint16_t *buffer, uint32_t x_pos, uint32_t y_pos);
static uint32_t display_metric_text(uint16_t *buffer);
static uint32_t display_logo(uint16_t *buffer);
static uint32_t display_title_text(uint16_t *buffer);
static uint32_t display_subtitle_text(uint16_t *buffer);

/***************************************************************************
*   Function: display_task_init
*
*   Description:
*       Initialize the display
*
*   Parameters:
*       none
*
*   Return:
*       Success or not
*/
uint32_t display_task_init(void)
{
    char *device = NULL;
    char *module = "rcar-du";
    uint32_t index = 0;
    uint16_t *buffer_plane_0;
    uint8_t y;
    uint8_t uv;
    uint32_t i;
    uint32_t j;

   /* Initialize device information to 0 */
    memset(&dev, 0, sizeof dev);

    /* Set only one plane */
    plane_count = 1;

    /* Check number of streams */
    if (BENCHMARK_NB_CAMERAS != 4) /* 4 cameras are used for the display task */
    {
        fprintf (stderr, "The number of cameras (for the display) must be equal to 4\n");
        return 1;
    }

    /* Allocate memory for planes */
    for (i = 0; i < plane_count; ++i)
    {
        plane_args = realloc (plane_args, (plane_count + 1) * sizeof(*plane_args));
        if (plane_args == NULL)
        {
            fprintf (stderr, "memory allocation failed\n");
            return 1;
        }
        memset (&plane_args[plane_count], 0, sizeof(*plane_args));
    }

    /* Set plane 0 information */
    index = 0;
    plane_args[index].plane_id      = 34;
    plane_args[index].crtc_id       = 54;
    plane_args[index].w             = DISPLAY_WIDTH;
    plane_args[index].h             = DISPLAY_HEIGHT;
    plane_args[index].x             = 0;
    plane_args[index].y             = 0;
    plane_args[index].has_position  = false;
    plane_args[index].scale         = 1.0;
    strcpy (plane_args[index].format_str, "YUYV");
    plane_args[index].fourcc        = util_format_fourcc (plane_args[index].format_str);
    if (plane_args[index].fourcc == 0)
    {
        fprintf (stderr, "unknown format %s\n", plane_args[index].format_str);
        return -EINVAL;
    }

    /* Open device */
    dev.fd = util_open (device, module);
    if (dev.fd < 0)
        return -1;

    /* Get resources (plane id , crtc id etc.) */
    dev.resources = get_resources (&dev);
    if (!dev.resources)
    {
        drmClose (dev.fd);
        return 1;
    }

    /* Set the planes */
    set_planes (&dev, plane_args, plane_count);

    /* Get planes */
    buffer_plane_0 = (uint16_t*) get_buffer (plane_args[0].bo);

    /* Set background image */
    uint8_t *background_image = malloc((DISPLAY_WIDTH * DISPLAY_HEIGHT) * (RGB_NB_PLAN * sizeof(uint8_t)));

    if (image_u8_ppm_read(BENCHMARK_INPUT_BACKGROUND_SCREEN, background_image) != 0)
    {
        fprintf (stderr, "Failed at function image_u8_ppm_read\n");
    }

    for (i = 0; i < DISPLAY_HEIGHT; ++i)
    {
        for (j = 0; j < DISPLAY_WIDTH; ++j)
        {
            y = MAKE_YUV_601_Y(
                    (uint32_t)background_image[(((i * DISPLAY_WIDTH) + j) * RGB_NB_PLAN) + 0],
                    (uint32_t)background_image[(((i * DISPLAY_WIDTH) + j) * RGB_NB_PLAN) + 1],
                    (uint32_t)background_image[(((i * DISPLAY_WIDTH) + j) * RGB_NB_PLAN) + 2]);
            uv = ((j % 2) == 0)
                ?
                MAKE_YUV_601_U(
                    (uint32_t)background_image[(((i * DISPLAY_WIDTH) + j) * RGB_NB_PLAN) + 0],
                    (uint32_t)background_image[(((i * DISPLAY_WIDTH) + j) * RGB_NB_PLAN) + 1],
                    (uint32_t)background_image[(((i * DISPLAY_WIDTH) + j) * RGB_NB_PLAN) + 2])
                :
                MAKE_YUV_601_V(
                    (uint32_t)background_image[(((i * DISPLAY_WIDTH) + j) * RGB_NB_PLAN) + 0],
                    (uint32_t)background_image[(((i * DISPLAY_WIDTH) + j) * RGB_NB_PLAN) + 1],
                    (uint32_t)background_image[(((i * DISPLAY_WIDTH) + j) * RGB_NB_PLAN) + 2]);
            buffer_plane_0[(i * DISPLAY_WIDTH) + j] = (y << 0) + (uv << 8);
        }
    }

    /* Add rectangles for titles ISP */
    if (display_rectangle (buffer_plane_0, TITLE_RECT_START_X, TITLE_RECT_START_ISP_Y, TITLE_RECT_WIDTH,
                           TITLE_RECT_HEIGHT, TITLE_RECT_CORNER) != 0)
    {
        printf ("Failed display_rectangle\n");
        return 1;
    }

    /* Add rectangles for titles DOF */
    if (display_rectangle (buffer_plane_0, TITLE_RECT_START_X, TITLE_RECT_START_DOF_Y, TITLE_RECT_WIDTH,
                           TITLE_RECT_HEIGHT, TITLE_RECT_CORNER) != 0)
    {
        printf ("Failed display_rectangle\n");
        return 1;
    }

    /* Add a rectangle for the metric part */
    if (display_rectangle (buffer_plane_0, METRIC_RECT_START_X, METRIC_RECT_START_Y, METRIC_RECT_WIDTH,
                           METRIC_RECT_HEIGHT, METRIC_RECT_CORNER) != 0)
    {
        printf ("Failed display_rectangle\n");
        return 1;
    }

    /* Add a rectangle for the sub title of each frame */
    if (display_rectangle (buffer_plane_0, STITLE_RECT_ISP_0_X, STITLE_RECT_ISP_Y, STITLE_RECT_WIDTH,
                           STITLE_RECT_HEIGHT, STITLE_RECT_CORNER) != 0)
    {
        printf ("Failed display_rectangle\n");
        return 1;
    }
    if (display_rectangle (buffer_plane_0, STITLE_RECT_ISP_1_X, STITLE_RECT_ISP_Y, STITLE_RECT_WIDTH,
                           STITLE_RECT_HEIGHT, STITLE_RECT_CORNER) != 0)
    {
        printf ("Failed display_rectangle\n");
        return 1;
    }
    if (display_rectangle (buffer_plane_0, STITLE_RECT_ISP_2_X, STITLE_RECT_ISP_Y, STITLE_RECT_WIDTH,
                           STITLE_RECT_HEIGHT, STITLE_RECT_CORNER) != 0)
    {
        printf ("Failed display_rectangle\n");
        return 1;
    }
    if (display_rectangle (buffer_plane_0, STITLE_RECT_ISP_3_X, STITLE_RECT_ISP_Y, STITLE_RECT_WIDTH,
                           STITLE_RECT_HEIGHT, STITLE_RECT_CORNER) != 0)
    {
        printf ("Failed display_rectangle\n");
        return 1;
    }


    if (display_rectangle (buffer_plane_0, STITLE_RECT_DOF_0_X, STITLE_RECT_DOF_Y, STITLE_RECT_WIDTH,
                           STITLE_RECT_HEIGHT, STITLE_RECT_CORNER) != 0)
    {
        printf ("Failed display_rectangle\n");
        return 1;
    }
    if (display_rectangle (buffer_plane_0, STITLE_RECT_DOF_1_X, STITLE_RECT_DOF_Y, STITLE_RECT_WIDTH,
                           STITLE_RECT_HEIGHT, STITLE_RECT_CORNER) != 0)
    {
        printf ("Failed display_rectangle\n");
        return 1;
    }
    if (display_rectangle (buffer_plane_0, STITLE_RECT_DOF_2_X, STITLE_RECT_DOF_Y, STITLE_RECT_WIDTH,
                           STITLE_RECT_HEIGHT, STITLE_RECT_CORNER) != 0)
    {
        printf ("Failed display_rectangle\n");
        return 1;
    }
    if (display_rectangle (buffer_plane_0, STITLE_RECT_DOF_3_X, STITLE_RECT_DOF_Y, STITLE_RECT_WIDTH,
                           STITLE_RECT_HEIGHT, STITLE_RECT_CORNER) != 0)
    {
        printf ("Failed display_rectangle\n");
        return 1;
    }

    /* Add text for the sub title part */
    if (display_subtitle_text (buffer_plane_0) != 0)
    {
        printf ("Failed display_subtitle_text\n");
        return 1;
    }

    /* Add text for the metric part */
    if (display_metric_text (buffer_plane_0) != 0)
    {
        printf ("Failed display_metric_text\n");
        return 1;
    }

    /* Add text for the title part */
    if (display_title_text (buffer_plane_0) != 0)
    {
        printf ("Failed display_title_text\n");
        return 1;
    }

    /* Add logo */
    if (display_logo (buffer_plane_0) != 0)
    {
        printf ("Failed display_metric_text\n");
        return 1;
    }

    return 0;
}

/***************************************************************************
*   Function: display_task_run
*
*   Description:
*       Print the data on the display
*
*   Parameters:
*       history_id - history index to save two sequential frames
*
*   Return:
*       Success or not
*/
uint32_t display_task_run(uint8_t history_id)
{
    /*
     * Create the display for 4 different streams
     *
     * FULL HD resolution
     *
     *          <------- 1920 ------->
     *          ----------------------  ˄
     *          |                    |  |
     *          |                    |  |
     *          |                    | 1080
     *          |                    |  |
     *          |                    |  |
     *          ----------------------  ˅
     *
     *
     * The area will be split in 3 sub areas:
     *      - SA1: 1920x380  4 outputs ISP streams
     *      - SA2: 1920x380  4 outputs DOF streams
     *      - SA3: 1920x320  metrics (fps etc.)
     *
     * SA1:
     *      - Each image will have a size of 300x300
     *      - One rectangle on left side to add the title of the sequence
     *      - One rectangle under each image to indicate the core used for the process
     *      - Line example: 50 + Rectangle + 50 + Image0 + 40 + Image1 + 40 + Image2 + 40 + Image3 + 300 = 1920
     *      - Column example: 40 + Image + 40 = 380
     *
     * SA2:
     *      - Each image will have a size of 300x300
     *      - One rectangle on left side to add the title of the sequence
     *      - One rectangle under each image to indicate the core used for the process
     *      - Line example: 50 + Rectangle + 50 + Image0 + 40 + Image1 + 40 + Image2 + 40 + Image3 + 300 = 1920
     *      - Column example: 40 + Image + 40 = 380
     *
     * SA3:
     *      - Print "FPS", LOAD RATE", "BANDWIDTH" metrics
     *
     */

    e_osal_return_t ret_osal;
    void *isp_output[BENCHMARK_NB_CAMERAS];
    void *imr_output[BENCHMARK_NB_CAMERAS];
    void *dof_l1_output[BENCHMARK_NB_CAMERAS];
    void *dof_l2_output[BENCHMARK_NB_CAMERAS];
    uint8_t y;
    uint8_t uv;
    uint8_t r = 0;
    uint8_t g = 0;
    uint8_t b = 0;
    uint32_t i;
    uint32_t j;
    uint32_t k;
    uint64_t time_total;
    uint32_t xpos_sa1_image[BENCHMARK_NB_CAMERAS] = {SUB_IMAGE_ISP_0_X, SUB_IMAGE_ISP_1_X, SUB_IMAGE_ISP_2_X,
                                               SUB_IMAGE_ISP_3_X};
    uint32_t xpos_sa1_metric[BENCHMARK_NB_CAMERAS] = {STITLE_RECT_ISP_0_X, STITLE_RECT_ISP_1_X, STITLE_RECT_ISP_2_X,
                                               STITLE_RECT_ISP_3_X};
    uint32_t xpos_sa2_image[BENCHMARK_NB_CAMERAS] = {SUB_IMAGE_DOF_0_X, SUB_IMAGE_DOF_1_X, SUB_IMAGE_DOF_2_X,
                                                     SUB_IMAGE_DOF_3_X};
    uint32_t xpos_sa2_metric[BENCHMARK_NB_CAMERAS] = {STITLE_RECT_DOF_0_X, STITLE_RECT_DOF_1_X, STITLE_RECT_DOF_2_X,
                                                      STITLE_RECT_DOF_3_X};
    float fps;
    float read_rate = 0;
    float write_rate = 0;
    float avg[BENCHMARK_NB_CAMERAS] = {0};
    float nb_vectors[BENCHMARK_NB_CAMERAS] = {0};
    float vector_size;

    /* Get pointer to the plane */
    uint16_t *buffer = (uint16_t*) get_buffer (plane_args[0].bo);

    /* Get virtual address from processed buffer */
    for (i = 0; i < BENCHMARK_NB_CAMERAS; ++i)
    {
        ret_osal = R_OSAL_MmngrInvalidate (frame_resolution.frame_buffer_after_isp[i], 0,
                                           frame_resolution.frame_out_size_isp);
        if (ret_osal != OSAL_RETURN_OK)
        {
            printf ("Failed R_OSAL_MmngrInvalidate ret=%d\n", ret_osal);
            return -1;
        }

        ret_osal = R_OSAL_MmngrGetCpuPtr (frame_resolution.frame_buffer_after_isp[i], &isp_output[i]);
        if (ret_osal != OSAL_RETURN_OK)
        {
            printf ("Failed R_OSAL_MmngrGetCpuPtr ret=%d\n", ret_osal);
            return 1;
        }

        ret_osal = R_OSAL_MmngrInvalidate (frame_resolution.frame_buffer_after_imr[i][history_id], 0,
                                           frame_resolution.frame_out_size_imr);
        if (ret_osal != OSAL_RETURN_OK)
        {
            printf ("Failed R_OSAL_MmngrInvalidate ret=%d\n", ret_osal);
            return -1;
        }

        ret_osal = R_OSAL_MmngrGetCpuPtr (frame_resolution.frame_buffer_after_imr[i][history_id], &imr_output[i]);
        if (ret_osal != OSAL_RETURN_OK)
        {
            printf ("Failed R_OSAL_MmngrGetCpuPtr ret=%d\n", ret_osal);
            return 1;
        }

        ret_osal = R_OSAL_MmngrInvalidate (frame_resolution.frame_buffer_after_dof_l1[i], 0,
                                           frame_resolution.frame_out_size_dof_l1);
        if (ret_osal != OSAL_RETURN_OK)
        {
            printf ("Failed R_OSAL_MmngrInvalidate ret=%d\n", ret_osal);
            return -1;
        }

        ret_osal = R_OSAL_MmngrGetCpuPtr (frame_resolution.frame_buffer_after_dof_l1[i], &dof_l1_output[i]);
        if (ret_osal != OSAL_RETURN_OK)
        {
            printf ("Failed R_OSAL_MmngrGetCpuPtr ret=%d\n", ret_osal);
            return 1;
        }

        ret_osal = R_OSAL_MmngrInvalidate (frame_resolution.frame_buffer_after_dof_l2[i], 0,
                                           frame_resolution.frame_out_size_dof_l2);
        if (ret_osal != OSAL_RETURN_OK)
        {
            printf ("Failed R_OSAL_MmngrInvalidate ret=%d\n", ret_osal);
            return -1;
        }

        ret_osal = R_OSAL_MmngrGetCpuPtr (frame_resolution.frame_buffer_after_dof_l2[i], &dof_l2_output[i]);
        if (ret_osal != OSAL_RETURN_OK)
        {
            printf ("Failed R_OSAL_MmngrGetCpuPtr ret=%d\n", ret_osal);
            return 1;
        }
    }

    /* Compute fps and global run time */
    time_total = kpi_results.time_end - kpi_results.time_start; /* microsecond */
    fps = 1000000.0 / (float)time_total;
    fps = fps * BENCHMARK_NB_CAMERAS;

    /* Compute bandwidth rate */
    read_rate = ((float)kpi_results.bandwidth_read) / (float)time_total;
    write_rate = ((float)kpi_results.bandwidth_write) / (float)time_total;
    read_rate = (read_rate * 1000000) / (1024 * 1024); /* From kB/µs to GB/s */
    write_rate = (write_rate * 1000000) / (1024 * 1024); /* From kB/µs to GB/s */


    /** SA1 **/
    for (i = 0; i < SUB_IMAGE_HEIGHT; ++i)
    {
        for (j = 0; j < SUB_IMAGE_WIDTH; ++j)
        {
            for (k = 0; k < BENCHMARK_NB_CAMERAS; k++)
            {
                /* Image k. For offset value, please read comment at the beginning of the function. */
                y = ((((uint16_t*) isp_output[k])[((i * frame_resolution.frame_out_stride_isp) + j) * SUB_IMAGE_FACTOR + 0] +
                      ((uint16_t*) isp_output[k])[((i * frame_resolution.frame_out_stride_isp) + j) * SUB_IMAGE_FACTOR + 1] +
                      ((uint16_t*) isp_output[k])[((i * frame_resolution.frame_out_stride_isp) + j) * SUB_IMAGE_FACTOR + 2] +
                      ((uint16_t*) isp_output[k])[((i * frame_resolution.frame_out_stride_isp) + j) * SUB_IMAGE_FACTOR + 3]) / 4)
                      & 0xFF;
                uv = (((uint8_t*) isp_output[k])[(((i * SUB_IMAGE_FACTOR)
                        + (frame_resolution.frame_out_height_isp * (frame_resolution.frame_out_bpp_isp_y / 8)))
                        * frame_resolution.frame_out_stride_isp + (j * SUB_IMAGE_FACTOR)) + (j % 2)] +
                      ((uint8_t*) isp_output[k])[(((i * SUB_IMAGE_FACTOR)
                        + (frame_resolution.frame_out_height_isp * (frame_resolution.frame_out_bpp_isp_y / 8)))
                        * frame_resolution.frame_out_stride_isp + (j * SUB_IMAGE_FACTOR)) + (j % 2) + 2]) / 2;
                buffer[(SUB_IMAGE_ISP_Y + i) * DISPLAY_WIDTH + (xpos_sa1_image[k] + j)] = (y << 0) + (uv << 8);
            }
        }
    }

    for (k = 0; k < BENCHMARK_NB_CAMERAS; k++)
    {
        /* Display the core ID */
        if (display_number (
                (float) kpi_results.isp[k].core_number,
                2,
                buffer,
                xpos_sa1_metric[k] + (STITLE_RECT_WIDTH - TEXT_STITLE_ISP_WIDTH) / 2 + TEXT_STITLE_ISP_OFFEST_CORE_X,
                STITLE_RECT_ISP_Y + (STITLE_RECT_HEIGHT - TEXT_STITLE_ISP_HEIGHT) / 2) != 0)
        {
            printf ("Failed display_number\n");
            return 1;
        }
    }


    /** SA2 **/

    /* Compute sum of the vectors size in each image */
    for (i = 0; i < SUB_IMAGE_HEIGHT; ++i)
    {
        for (uint32_t j = 0; j < SUB_IMAGE_WIDTH; ++j)
        {
            for (k = 0; k < BENCHMARK_NB_CAMERAS; ++k)
            {
                vector_size = display_compute_vector_dof (dof_l2_output[k], dof_l1_output[k], j, i);
                avg[k] += vector_size;

                if (vector_size != 0)
                    nb_vectors[k]++;
            }
        }
    }

    /* Compute average of the vectors size */
    for (i = 0; i < BENCHMARK_NB_CAMERAS; ++i)
    {
        avg[i] = avg[i] / nb_vectors[i];
    }

    /* Display overlay on the output image */
    for (i = 0; i < SUB_IMAGE_HEIGHT; ++i)
    {
        for (j = 0; j < SUB_IMAGE_WIDTH; ++j)
        {
            for (k = 0; k < BENCHMARK_NB_CAMERAS; ++k)
            {
                /* Image k. For offset value, please read comment at the beginning of the function. */
                display_overlay_hsv(imr_output[k], dof_l2_output[k], dof_l1_output[k], j, i, &r, &g, &b, avg[k]);
                y = MAKE_YUV_601_Y((uint32_t)r, (uint32_t)g, (uint32_t)b);
                uv = ((j % 2) == 1) ? MAKE_YUV_601_U((uint32_t)r, (uint32_t)g, (uint32_t)b) :
                                      MAKE_YUV_601_V((uint32_t)r, (uint32_t)g, (uint32_t)b);
                buffer[(SUB_IMAGE_DOF_Y + i) * DISPLAY_WIDTH + (xpos_sa2_image[k] + j)] = (y << 0) + (uv << 8);
            }
        }
    }

    for (k = 0; k < BENCHMARK_NB_CAMERAS; ++k)
    {
        /* Display the core ID of sub image 0 */
        if (display_number (
                (float) kpi_results.imr_uv[k].core_number,
                2,
                buffer,
                xpos_sa2_metric[k] + (STITLE_RECT_WIDTH - TEXT_STITLE_DOF_WIDTH) / 2 + TEXT_STITLE_IMR_OFFEST_CORE_X,
                STITLE_RECT_DOF_Y + (STITLE_RECT_HEIGHT - TEXT_STITLE_DOF_HEIGHT) / 2) != 0)
        {
            printf ("Failed display_number\n");
            return 1;
        }
        if (display_number (
                (float) kpi_results.psc[k].core_number,
                2,
                buffer,
                xpos_sa2_metric[k] + (STITLE_RECT_WIDTH - TEXT_STITLE_DOF_WIDTH) / 2 + TEXT_STITLE_PSC_OFFEST_CORE_X,
                STITLE_RECT_DOF_Y + (STITLE_RECT_HEIGHT - TEXT_STITLE_DOF_HEIGHT) / 2) != 0)
        {
            printf ("Failed display_number\n");
            return 1;
        }
        if (display_number (
                (float) kpi_results.dof[k].core_number,
                2,
                buffer,
                xpos_sa2_metric[k] + (STITLE_RECT_WIDTH - TEXT_STITLE_DOF_WIDTH) / 2 + TEXT_STITLE_DOF_OFFEST_CORE_X,
                STITLE_RECT_DOF_Y + (STITLE_RECT_HEIGHT - TEXT_STITLE_DOF_HEIGHT) / 2) != 0)
        {
            printf ("Failed display_number\n");
            return 1;
        }
    }


    /** SA3 **/
    if (display_number(fps, 1, buffer, METRIC_FPS_VALUE_X, METRIC_FPS_Y) != 0)
    {
        printf ("Failed display_number\n");
        return 1;
    }

    if (display_number (read_rate, 3, buffer, METRIC_READ_X + TEXT_METRIC_READ_OFFEST_VALUE_X, METRIC_BANDWIDTH_Y) != 0)
    {
        printf ("Failed display_number\n");
        return 1;
    }

    if (display_number (write_rate, 3, buffer, METRIC_WRITE_X + TEXT_METRIC_WRITE_OFFEST_VALUE_X, METRIC_BANDWIDTH_Y)
            != 0)
    {
        printf ("Failed display_number\n");
        return 1;
    }

    if (display_number(kpi_results.load_rate_isp_group, 0, buffer, METRIC_ISP_X, METRIC_LOAD_RATE_Y) != 0)
    {
        printf ("Failed display_number\n");
        return 1;
    }

    if (display_number(kpi_results.load_rate_imr_group, 0, buffer, METRIC_IMR_X, METRIC_LOAD_RATE_Y) != 0)
    {
        printf ("Failed display_number\n");
        return 1;
    }

    if (display_number(kpi_results.load_rate_hog_group, 0, buffer, METRIC_HOG_X, METRIC_LOAD_RATE_Y) != 0)
    {
        printf ("Failed display_number\n");
        return 1;
    }

    if (display_number(kpi_results.load_rate_psc_group, 0, buffer, METRIC_PSC_X, METRIC_LOAD_RATE_Y) != 0)
    {
        printf ("Failed display_number\n");
        return 1;
    }

    if (display_number(kpi_results.load_rate_dof_group, 0, buffer, METRIC_DOF_X, METRIC_LOAD_RATE_Y) != 0)
    {
        printf ("Failed display_number\n");
        return 1;
    }

    if (display_number(kpi_results.load_rate_cpu_group, 0, buffer, METRIC_CPU_X, METRIC_LOAD_RATE_Y) != 0)
    {
        printf ("Failed display_number\n");
        return 1;
    }

#if (BENCHMARK_SAVE_DISPLAY == 1)
    int ret;
    int file_save = 1;
    char save_path[FILENAME_LENGTH];

    ret = sprintf (save_path, "%s_%dx%d.yuv%d", DATA_OUT, DISPLAY_WIDTH, DISPLAY_HEIGHT, 16);
    if (0 >= ret)
    {
        printf ("Error at sprintf\n");
        return 1;
    }

    /* Open the save file (need the path "IMR_data/") */
    file_save = open (save_path, O_RDWR | O_CREAT);
    if ((-1) == file_save)
    {
        printf ("Error at open\n");
        return 1;
    }

    /* Write the content of the buffer in a file */
    ret = write (file_save, (void*) buffer, (DISPLAY_WIDTH * DISPLAY_HEIGHT) * sizeof(uint16_t));
    if ((-1) == ret)
    {
        printf ("Error at write\n");
        close (file_save);
        return 1;
    }

    /* Close the file */
    close (file_save);

#endif /* (BENCHMARK_SAVE_DISPLAY == 1) */
    return 0;
}

/***************************************************************************
*   Function: display_task_quit
*
*   Description:
*       Close the display
*
*   Parameters:
*       none
*
*   Return:
*       Success or not
*/
uint32_t display_task_quit(void)
{
    uint32_t i;

    /* Unmap planes */
    for (i = 0; i < plane_count; ++i)
    {
        unmap_bo (plane_args[i].bo);
    }

    /* Clear planes */
    if (plane_count)
    {
        clear_planes (&dev, plane_args, plane_count);
    }

    /* Free resources */
    free_resources (dev.resources);

    /* Close device */
    drmClose (dev.fd);

    return 0;
}

/***************************************************************************
*   Function: image_u8_ppm_read
*
*   Description:
*       Read input image
*
*   Parameters:
*       filename - name of the file
*       image - buffer
*
*   Return:
*       Success or not
*/
static uint32_t image_u8_ppm_read(const char *fileName, uint8_t *image)
{
    FILE *fp;
    uint32_t i, j;
    int lo;

    if ((fileName == NULL ) || (image == NULL ))
    {
        printf("Pointer null\n");
        return 1;
    }

    fp = fopen (fileName, "rb");
    if (fp == NULL)
    {
        printf("Failed to open file %s\n", fileName);
        return 1;
    }

    for (i = 0; i < DISPLAY_HEIGHT; ++i)
    {
        for (j = 0; j < DISPLAY_WIDTH; ++j)
        {
            lo = fgetc (fp);
            *(uint8_t*) (image + (i * DISPLAY_WIDTH + j) * 3 + 0) = (uint8_t) lo;
            lo = fgetc (fp);
            *(uint8_t*) (image + (i * DISPLAY_WIDTH + j) * 3 + 1) = (uint8_t) lo;
            lo = fgetc (fp);
            *(uint8_t*) (image + (i * DISPLAY_WIDTH + j) * 3 + 2) = (uint8_t) lo;
        }
    }

    fclose (fp);

    return 0;
}

/***************************************************************************
*   Function: display_number
*
*   Description:
*       Display number
*
*   Parameters:
*       num - number to display
*       select_fps - 0 to display "load rate"
*                    1 to display "fps"
*                    2 to display "cores"
*                    3 to display bandwidth read and write values
*       buffer - output buffer
*       x_pos - x position in output buffer
*       y_pos - y position in output buffer
*
*   Return:
*       Success or not
*/
static uint32_t display_number(float num, uint8_t select_fps, uint16_t *buffer, uint32_t x_pos, uint32_t y_pos)
{
    char num_buffer[50];
    uint8_t *input_buffer;
    uint32_t test_digit_width = 0;
    uint32_t test_digit_height = 0;
    uint32_t test_digit_per_height = 0;
    uint32_t test_digit_offset_0 = 0;
    uint32_t test_digit_offset_1 = 0;
    uint32_t test_digit_offset_2 = 0;
    uint32_t test_digit_offset_3 = 0;
    uint32_t test_digit_offset_4 = 0;
    uint32_t test_digit_offset_5 = 0;
    uint32_t test_digit_offset_6 = 0;
    uint32_t test_digit_offset_7 = 0;
    uint32_t test_digit_offset_8 = 0;
    uint32_t test_digit_offset_9 = 0;
    uint32_t test_digit_offset_dot = 0;
    uint32_t test_digit_offset_per = 0;
    uint8_t y;
    uint8_t uv;
    uint8_t u;
    uint8_t v;
    uint8_t value;
    uint32_t i;
    uint32_t j;
    uint32_t k;
    uint32_t n = 0;
    uint32_t num_integer;
    uint32_t num_frac;

    switch (select_fps)
    {
        case 0: /* Load rate */
            num_integer = (uint32_t) num;
            num_frac = (uint32_t) ((num - num_integer) * 10);

            input_buffer = digit;
            test_digit_width = TEXT_DIGIT_WIDTH;
            test_digit_height = TEXT_DIGIT_HEIGHT;
            test_digit_per_height = TEXT_DIGIT_PER_HEIGHT;
            test_digit_offset_0 = TEXT_DIGIT_OFFSET_0;
            test_digit_offset_1 = TEXT_DIGIT_OFFSET_1;
            test_digit_offset_2 = TEXT_DIGIT_OFFSET_2;
            test_digit_offset_3 = TEXT_DIGIT_OFFSET_3;
            test_digit_offset_4 = TEXT_DIGIT_OFFSET_4;
            test_digit_offset_5 = TEXT_DIGIT_OFFSET_5;
            test_digit_offset_6 = TEXT_DIGIT_OFFSET_6;
            test_digit_offset_7 = TEXT_DIGIT_OFFSET_7;
            test_digit_offset_8 = TEXT_DIGIT_OFFSET_8;
            test_digit_offset_9 = TEXT_DIGIT_OFFSET_9;
            test_digit_offset_dot = TEXT_DIGIT_OFFSET_DOT;
            test_digit_offset_per = TEXT_DIGIT_OFFSET_PER;

            /* Check the range of the load rate */
            if (num_integer > 100)
            {
                printf ("Error: The load rate exceed 100%%\n");
                return -1;
            }

            /* Create string */
            n = sprintf (num_buffer, "%2d%%", num_integer);
        break;
        case 1: /* Fps */
            num_integer = (uint32_t) num;
            num_frac = (uint32_t) ((num - num_integer) * 10);

            input_buffer = digit;
            test_digit_width = TEXT_DIGIT_WIDTH;
            test_digit_height = TEXT_DIGIT_HEIGHT;
            test_digit_per_height = TEXT_DIGIT_PER_HEIGHT;
            test_digit_offset_0 = TEXT_DIGIT_OFFSET_0;
            test_digit_offset_1 = TEXT_DIGIT_OFFSET_1;
            test_digit_offset_2 = TEXT_DIGIT_OFFSET_2;
            test_digit_offset_3 = TEXT_DIGIT_OFFSET_3;
            test_digit_offset_4 = TEXT_DIGIT_OFFSET_4;
            test_digit_offset_5 = TEXT_DIGIT_OFFSET_5;
            test_digit_offset_6 = TEXT_DIGIT_OFFSET_6;
            test_digit_offset_7 = TEXT_DIGIT_OFFSET_7;
            test_digit_offset_8 = TEXT_DIGIT_OFFSET_8;
            test_digit_offset_9 = TEXT_DIGIT_OFFSET_9;
            test_digit_offset_dot = TEXT_DIGIT_OFFSET_DOT;
            test_digit_offset_per = TEXT_DIGIT_OFFSET_PER;

            /* Create string */
            n = sprintf (num_buffer, "%2d.%1d", num_integer, num_frac);
        break;
        case 2: /* Cores */
            num_integer = (uint32_t) num;
            num_frac = (uint32_t) ((num - num_integer) * 10);

            input_buffer = digit_minus;
            test_digit_width = TEXT_DIGIT_MINUS_WIDTH;
            test_digit_height = TEXT_DIGIT_MINUS_HEIGHT;
            test_digit_per_height = TEXT_DIGIT_MINUS_PER_HEIGHT;
            test_digit_offset_0 = TEXT_DIGIT_MINUS_OFFSET_0;
            test_digit_offset_1 = TEXT_DIGIT_MINUS_OFFSET_1;
            test_digit_offset_2 = TEXT_DIGIT_MINUS_OFFSET_2;
            test_digit_offset_3 = TEXT_DIGIT_MINUS_OFFSET_3;
            test_digit_offset_4 = TEXT_DIGIT_MINUS_OFFSET_4;
            test_digit_offset_5 = TEXT_DIGIT_MINUS_OFFSET_5;
            test_digit_offset_6 = TEXT_DIGIT_MINUS_OFFSET_6;
            test_digit_offset_7 = TEXT_DIGIT_MINUS_OFFSET_7;
            test_digit_offset_8 = TEXT_DIGIT_MINUS_OFFSET_8;
            test_digit_offset_9 = TEXT_DIGIT_MINUS_OFFSET_9;
            test_digit_offset_dot = TEXT_DIGIT_MINUS_OFFSET_DOT;
            test_digit_offset_per = TEXT_DIGIT_MINUS_OFFSET_PER;

            /* Create string */
            n = sprintf (num_buffer, "%d", num_integer);
        break;
        case 3: /* Bandwidth read and write */
            num_integer = (uint32_t) num;
            num_frac = (uint32_t) ((num - num_integer) * 10);

            input_buffer = digit;
            test_digit_width = TEXT_DIGIT_WIDTH;
            test_digit_height = TEXT_DIGIT_HEIGHT;
            test_digit_per_height = TEXT_DIGIT_PER_HEIGHT;
            test_digit_offset_0 = TEXT_DIGIT_OFFSET_0;
            test_digit_offset_1 = TEXT_DIGIT_OFFSET_1;
            test_digit_offset_2 = TEXT_DIGIT_OFFSET_2;
            test_digit_offset_3 = TEXT_DIGIT_OFFSET_3;
            test_digit_offset_4 = TEXT_DIGIT_OFFSET_4;
            test_digit_offset_5 = TEXT_DIGIT_OFFSET_5;
            test_digit_offset_6 = TEXT_DIGIT_OFFSET_6;
            test_digit_offset_7 = TEXT_DIGIT_OFFSET_7;
            test_digit_offset_8 = TEXT_DIGIT_OFFSET_8;
            test_digit_offset_9 = TEXT_DIGIT_OFFSET_9;
            test_digit_offset_dot = TEXT_DIGIT_OFFSET_DOT;
            test_digit_offset_per = TEXT_DIGIT_OFFSET_PER;

            /* Create string */
            n = sprintf (num_buffer, "%1d.%1d", num_integer, num_frac);
        break;
        default:
        break;
    }

    /* Add character on the display */
    for (i = 0; i < n; ++i)
    {
        switch (num_buffer[i])
        {
            case '0':
                for (j = 0; j < test_digit_height; ++j)
                {
                    for (k = 0; k < test_digit_width; ++k)
                    {
                        value = input_buffer[(j * test_digit_width) + (k + test_digit_offset_0)];

                        y = ((TEXT_COLOR_Y * value) + (METRIC_COLOR_Y * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
                        u = ((TEXT_COLOR_U * value) + (METRIC_COLOR_U * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
                        v = ((TEXT_COLOR_V * value) + (METRIC_COLOR_V * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
                        uv = (((((y_pos + j) * DISPLAY_WIDTH) + (x_pos + k)) % 2) == 0) ? u : v;

                        buffer[((y_pos + j) * DISPLAY_WIDTH) + (x_pos + k)] = (y << 0) + (uv << 8);
                    }
                }

                x_pos += test_digit_width;
            break;
            case '1':
                for (j = 0; j < test_digit_height; ++j)
                {
                    for (k = 0; k < test_digit_width; ++k)
                    {
                        value = input_buffer[(j * test_digit_width) + (k + test_digit_offset_1)];

                        y = ((TEXT_COLOR_Y * value) + (METRIC_COLOR_Y * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
                        u = ((TEXT_COLOR_U * value) + (METRIC_COLOR_U * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
                        v = ((TEXT_COLOR_V * value) + (METRIC_COLOR_V * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
                        uv = (((((y_pos + j) * DISPLAY_WIDTH) + (x_pos + k)) % 2) == 0) ? u : v;

                        buffer[((y_pos + j) * DISPLAY_WIDTH) + (x_pos + k)] = (y << 0) + (uv << 8);
                    }
                }

                x_pos += test_digit_width;
            break;
            case '2':
                for (j = 0; j < test_digit_height; ++j)
                {
                    for (k = 0; k < test_digit_width; ++k)
                    {
                        value = input_buffer[(j * test_digit_width) + (k + test_digit_offset_2)];

                        y = ((TEXT_COLOR_Y * value) + (METRIC_COLOR_Y * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
                        u = ((TEXT_COLOR_U * value) + (METRIC_COLOR_U * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
                        v = ((TEXT_COLOR_V * value) + (METRIC_COLOR_V * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
                        uv = (((((y_pos + j) * DISPLAY_WIDTH) + (x_pos + k)) % 2) == 0) ? u : v;

                        buffer[((y_pos + j) * DISPLAY_WIDTH) + (x_pos + k)] = (y << 0) + (uv << 8);
                    }
                }

                x_pos += test_digit_width;
            break;
            case '3':
                for (j = 0; j < test_digit_height; ++j)
                {
                    for (k = 0; k < test_digit_width; ++k)
                    {
                        value = input_buffer[(j * test_digit_width) + (k + test_digit_offset_3)];

                        y = ((TEXT_COLOR_Y * value) + (METRIC_COLOR_Y * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
                        u = ((TEXT_COLOR_U * value) + (METRIC_COLOR_U * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
                        v = ((TEXT_COLOR_V * value) + (METRIC_COLOR_V * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
                        uv = (((((y_pos + j) * DISPLAY_WIDTH) + (x_pos + k)) % 2) == 0) ? u : v;

                        buffer[((y_pos + j) * DISPLAY_WIDTH) + (x_pos + k)] = (y << 0) + (uv << 8);
                    }
                }

                x_pos += test_digit_width;
            break;
            case '4':
                for (j = 0; j < test_digit_height; ++j)
                {
                    for (k = 0; k < test_digit_width; ++k)
                    {
                        value = input_buffer[(j * test_digit_width) + (k + test_digit_offset_4)];

                        y = ((TEXT_COLOR_Y * value) + (METRIC_COLOR_Y * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
                        u = ((TEXT_COLOR_U * value) + (METRIC_COLOR_U * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
                        v = ((TEXT_COLOR_V * value) + (METRIC_COLOR_V * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
                        uv = (((((y_pos + j) * DISPLAY_WIDTH) + (x_pos + k)) % 2) == 0) ? u : v;

                        buffer[((y_pos + j) * DISPLAY_WIDTH) + (x_pos + k)] = (y << 0) + (uv << 8);
                    }
                }

                x_pos += test_digit_width;
            break;
            case '5':
                for (j = 0; j < test_digit_height; ++j)
                {
                    for (k = 0; k < test_digit_width; ++k)
                    {
                        value = input_buffer[(j * test_digit_width) + (k + test_digit_offset_5)];

                        y = ((TEXT_COLOR_Y * value) + (METRIC_COLOR_Y * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
                        u = ((TEXT_COLOR_U * value) + (METRIC_COLOR_U * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
                        v = ((TEXT_COLOR_V * value) + (METRIC_COLOR_V * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
                        uv = (((((y_pos + j) * DISPLAY_WIDTH) + (x_pos + k)) % 2) == 0) ? u : v;

                        buffer[((y_pos + j) * DISPLAY_WIDTH) + (x_pos + k)] = (y << 0) + (uv << 8);
                    }
                }

                x_pos += test_digit_width;
            break;
            case '6':
                for (j = 0; j < test_digit_height; ++j)
                {
                    for (k = 0; k < test_digit_width; ++k)
                    {
                        value = input_buffer[(j * test_digit_width) + (k + test_digit_offset_6)];

                        y = ((TEXT_COLOR_Y * value) + (METRIC_COLOR_Y * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
                        u = ((TEXT_COLOR_U * value) + (METRIC_COLOR_U * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
                        v = ((TEXT_COLOR_V * value) + (METRIC_COLOR_V * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
                        uv = (((((y_pos + j) * DISPLAY_WIDTH) + (x_pos + k)) % 2) == 0) ? u : v;

                        buffer[((y_pos + j) * DISPLAY_WIDTH) + (x_pos + k)] = (y << 0) + (uv << 8);
                    }
                }

                x_pos += test_digit_width;
            break;
            case '7':
                for (j = 0; j < test_digit_height; ++j)
                {
                    for (k = 0; k < test_digit_width; ++k)
                    {
                        value = input_buffer[(j * test_digit_width) + (k + test_digit_offset_7)];

                        y = ((TEXT_COLOR_Y * value) + (METRIC_COLOR_Y * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
                        u = ((TEXT_COLOR_U * value) + (METRIC_COLOR_U * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
                        v = ((TEXT_COLOR_V * value) + (METRIC_COLOR_V * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
                        uv = (((((y_pos + j) * DISPLAY_WIDTH) + (x_pos + k)) % 2) == 0) ? u : v;

                        buffer[((y_pos + j) * DISPLAY_WIDTH) + (x_pos + k)] = (y << 0) + (uv << 8);
                    }
                }

                x_pos += test_digit_width;
            break;
            case '8':
                for (j = 0; j < test_digit_height; ++j)
                {
                    for (k = 0; k < test_digit_width; ++k)
                    {
                        value = input_buffer[(j * test_digit_width) + (k + test_digit_offset_8)];

                        y = ((TEXT_COLOR_Y * value) + (METRIC_COLOR_Y * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
                        u = ((TEXT_COLOR_U * value) + (METRIC_COLOR_U * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
                        v = ((TEXT_COLOR_V * value) + (METRIC_COLOR_V * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
                        uv = (((((y_pos + j) * DISPLAY_WIDTH) + (x_pos + k)) % 2) == 0) ? u : v;

                        buffer[((y_pos + j) * DISPLAY_WIDTH) + (x_pos + k)] = (y << 0) + (uv << 8);
                    }
                }

                x_pos += test_digit_width;
            break;
            case '9':
                for (j = 0; j < test_digit_height; ++j)
                {
                    for (k = 0; k < test_digit_width; ++k)
                    {
                        value = input_buffer[(j * test_digit_width) + (k + test_digit_offset_9)];

                        y = ((TEXT_COLOR_Y * value) + (METRIC_COLOR_Y * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
                        u = ((TEXT_COLOR_U * value) + (METRIC_COLOR_U * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
                        v = ((TEXT_COLOR_V * value) + (METRIC_COLOR_V * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
                        uv = (((((y_pos + j) * DISPLAY_WIDTH) + (x_pos + k)) % 2) == 0) ? u : v;

                        buffer[((y_pos + j) * DISPLAY_WIDTH) + (x_pos + k)] = (y << 0) + (uv << 8);
                    }
                }

                x_pos += test_digit_width;
            break;
            case ' ':
                for (j = 0; j < test_digit_height; ++j)
                {
                    for (k = 0; k < test_digit_width; ++k)
                    {
                        y = METRIC_COLOR_Y;
                        u = METRIC_COLOR_U;
                        v = METRIC_COLOR_V;
                        uv = (((((y_pos + j) * DISPLAY_WIDTH) + (x_pos + k)) % 2) == 0) ? u : v;

                        buffer[((y_pos + j) * DISPLAY_WIDTH) + (x_pos + k)] = (y << 0) + (uv << 8);
                    }
                }

                x_pos += test_digit_width;
            break;
            case '.':
                for (j = 0; j < test_digit_height; ++j)
                {
                    for (k = 0; k < test_digit_width; ++k)
                    {
                        value = input_buffer[(j * test_digit_width) + (k + test_digit_offset_dot)];

                        y = ((TEXT_COLOR_Y * value) + (METRIC_COLOR_Y * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
                        u = ((TEXT_COLOR_U * value) + (METRIC_COLOR_U * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
                        v = ((TEXT_COLOR_V * value) + (METRIC_COLOR_V * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
                        uv = (((((y_pos + j) * DISPLAY_WIDTH) + (x_pos + k)) % 2) == 0) ? u : v;

                        buffer[((y_pos + j) * DISPLAY_WIDTH) + (x_pos + k)] = (y << 0) + (uv << 8);
                    }
                }

                x_pos += test_digit_width;
            break;
            case '%':
                for (j = 0; j < test_digit_per_height; ++j)
                {
                    for (k = 0; k < test_digit_width; ++k)
                    {
                        value = input_buffer[(j * test_digit_width) + (k + test_digit_offset_per)];

                        y = ((TEXT_COLOR_Y * value) + (METRIC_COLOR_Y * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
                        u = ((TEXT_COLOR_U * value) + (METRIC_COLOR_U * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
                        v = ((TEXT_COLOR_V * value) + (METRIC_COLOR_V * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
                        uv = ((((((y_pos - (test_digit_per_height - test_digit_height)) + j) * DISPLAY_WIDTH)
                                + (x_pos + k)) % 2) == 0) ? u : v;

                        buffer[(((y_pos - (test_digit_per_height - test_digit_height)) + j) * DISPLAY_WIDTH)
                                + (x_pos + k)] = (y << 0) + (uv << 8);
                    }
                }

                x_pos += test_digit_width;
            break;
            default:
                printf ("Character not recognized \"%c\"\n", num_buffer[i]);
            break;
        }
    }
    return 0;
}

/***************************************************************************
*   Function: display_metric_text
*
*   Description:
*       Display number
*
*   Parameters:
*       buffer - output buffer
*
*   Return:
*       Success or not
*/
static uint32_t display_metric_text(uint16_t *buffer)
{
    uint8_t y;
    uint8_t uv;
    uint8_t u;
    uint8_t v;
    uint8_t value;
    uint32_t j;
    uint32_t k;

    /* Display "FPS" */
    for (j = 0; j < TEXT_DIGIT_HEIGHT; ++j)
    {
        for (k = 0; k < TEXT_METRIC_FPS_WIDTH; ++k)
        {
            value = text[(j * TEXT_METRIC_WIDTH) + (k + TEXT_METRIC_FPS_OFFSET)];

            y = ((TEXT_TITLE_COLOR_Y * value) + (METRIC_COLOR_Y * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            u = ((TEXT_TITLE_COLOR_U * value) + (METRIC_COLOR_U * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            v = ((TEXT_TITLE_COLOR_V * value) + (METRIC_COLOR_V * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            uv = (((((METRIC_FPS_Y + j) * DISPLAY_WIDTH) + (METRIC_FPS_X + k)) % 2) == 0) ? u : v;

            buffer[((METRIC_FPS_Y + j) * DISPLAY_WIDTH) + (METRIC_FPS_X + k)] = (y << 0) + (uv << 8);
        }
    }

    /* Display "BANDWIDTH" */
    for (j = 0; j < TEXT_DIGIT_HEIGHT; ++j)
    {
        for (k = 0; k < TEXT_METRIC_BANDWIDTH_WIDTH; ++k)
        {
            value = text[(j * TEXT_METRIC_WIDTH) + (k + TEXT_METRIC_BANDWIDTH_OFFSET)];

            y = ((TEXT_TITLE_COLOR_Y * value) + (METRIC_COLOR_Y * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            u = ((TEXT_TITLE_COLOR_U * value) + (METRIC_COLOR_U * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            v = ((TEXT_TITLE_COLOR_V * value) + (METRIC_COLOR_V * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            uv = (((((METRIC_BANDWIDTH_Y + j) * DISPLAY_WIDTH) + (METRIC_BANDWIDTH_X + k)) % 2) == 0) ? u : v;

            buffer[((METRIC_BANDWIDTH_Y + j) * DISPLAY_WIDTH) + (METRIC_BANDWIDTH_X + k)] = (y << 0) + (uv << 8);
        }
    }

    /* Display "READ" */
    for (j = 0; j < TEXT_DIGIT_HEIGHT; ++j)
    {
        for (k = 0; k < TEXT_METRIC_READ_WIDTH; ++k)
        {
            value = text[(j * TEXT_METRIC_WIDTH) + (k + TEXT_METRIC_READ_OFFSET)];

            y = ((TEXT_COLOR_Y * value) + (METRIC_COLOR_Y * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            u = ((TEXT_COLOR_U * value) + (METRIC_COLOR_U * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            v = ((TEXT_COLOR_V * value) + (METRIC_COLOR_V * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            uv = (((((METRIC_BANDWIDTH_Y + j) * DISPLAY_WIDTH) + (METRIC_READ_X + k)) % 2) == 0) ? u : v;

            buffer[((METRIC_BANDWIDTH_Y + j) * DISPLAY_WIDTH) + (METRIC_READ_X + k)] = (y << 0) + (uv << 8);
        }
    }

    /* Display "WRITE" */
    for (j = 0; j < TEXT_DIGIT_HEIGHT; ++j)
    {
        for (k = 0; k < TEXT_METRIC_WRITE_WIDTH; ++k)
        {
            value = text[(j * TEXT_METRIC_WIDTH) + (k + TEXT_METRIC_WRITE_OFFSET)];

            y = ((TEXT_COLOR_Y * value) + (METRIC_COLOR_Y * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            u = ((TEXT_COLOR_U * value) + (METRIC_COLOR_U * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            v = ((TEXT_COLOR_V * value) + (METRIC_COLOR_V * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            uv = (((((METRIC_BANDWIDTH_Y + j) * DISPLAY_WIDTH) + (METRIC_WRITE_X + k)) % 2) == 0) ? u : v;

            buffer[((METRIC_BANDWIDTH_Y + j) * DISPLAY_WIDTH) + (METRIC_WRITE_X + k)] = (y << 0) + (uv << 8);
        }
    }

    /* Display "LOAD RATE" */
    for (j = 0; j < TEXT_DIGIT_HEIGHT; ++j)
    {
        for (k = 0; k < TEXT_METRIC_LOAD_RATE_WIDTH; ++k)
        {
            value = text[(j * TEXT_METRIC_WIDTH) + (k + TEXT_METRIC_LOAD_RATE_OFFSET)];

            y = ((TEXT_TITLE_COLOR_Y * value) + (METRIC_COLOR_Y * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            u = ((TEXT_TITLE_COLOR_U * value) + (METRIC_COLOR_U * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            v = ((TEXT_TITLE_COLOR_V * value) + (METRIC_COLOR_V * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            uv = (((((METRIC_LOAD_RATE_Y + j) * DISPLAY_WIDTH) + (METRIC_LOAD_RATE_X + k)) % 2) == 0) ? u : v;

            buffer[((METRIC_LOAD_RATE_Y + j) * DISPLAY_WIDTH) + (METRIC_LOAD_RATE_X + k)] = (y << 0) + (uv << 8);
        }
    }

    /* Display "ISP" */
    for (j = 0; j < TEXT_DIGIT_HEIGHT; ++j)
    {
        for (k = 0; k < TEXT_METRIC_ISP_WIDTH; ++k)
        {
            value = text[(j * TEXT_METRIC_WIDTH) + (k + TEXT_METRIC_ISP_OFFSET)];

            y = ((TEXT_COLOR_Y * value) + (METRIC_COLOR_Y * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            u = ((TEXT_COLOR_U * value) + (METRIC_COLOR_U * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            v = ((TEXT_COLOR_V * value) + (METRIC_COLOR_V * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            uv = (((((METRIC_LOAD_RATE_Y + j) * DISPLAY_WIDTH) + ((METRIC_ISP_X - TEXT_METRIC_ISP_WIDTH) + k)) % 2)
                    == 0) ? u : v;

            buffer[((METRIC_LOAD_RATE_Y + j) * DISPLAY_WIDTH) + ((METRIC_ISP_X - TEXT_METRIC_ISP_WIDTH) + k)] =
                    (y << 0) + (uv << 8);
        }
    }

    /* Display "IMR" */
    for (j = 0; j < TEXT_DIGIT_HEIGHT; ++j)
    {
        for (k = 0; k < TEXT_METRIC_IMR_WIDTH; ++k)
        {

            value = text[(j * TEXT_METRIC_WIDTH) + (k + TEXT_METRIC_IMR_OFFSET)];

            y = ((TEXT_COLOR_Y * value) + (METRIC_COLOR_Y * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            u = ((TEXT_COLOR_U * value) + (METRIC_COLOR_U * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            v = ((TEXT_COLOR_V * value) + (METRIC_COLOR_V * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            uv = (((((METRIC_LOAD_RATE_Y + j) * DISPLAY_WIDTH) + ((METRIC_IMR_X - TEXT_METRIC_IMR_WIDTH) + k)) % 2)
                    == 0) ? u : v;

            buffer[((METRIC_LOAD_RATE_Y + j) * DISPLAY_WIDTH) + ((METRIC_IMR_X - TEXT_METRIC_IMR_WIDTH) + k)] =
                    (y << 0) + (uv << 8);
        }
    }

    /* Display "HOG" */
    for (j = 0; j < TEXT_DIGIT_HEIGHT; ++j)
    {
        for (k = 0; k < TEXT_METRIC_HOG_WIDTH; ++k)
        {
            value = text[(j * TEXT_METRIC_WIDTH) + (k + TEXT_METRIC_HOG_OFFSET)];

            y = ((TEXT_COLOR_Y * value) + (METRIC_COLOR_Y * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            u = ((TEXT_COLOR_U * value) + (METRIC_COLOR_U * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            v = ((TEXT_COLOR_V * value) + (METRIC_COLOR_V * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            uv = (((((METRIC_LOAD_RATE_Y + j) * DISPLAY_WIDTH) + ((METRIC_HOG_X - TEXT_METRIC_HOG_WIDTH) + k)) % 2)
                    == 0) ? u : v;

            buffer[((METRIC_LOAD_RATE_Y + j) * DISPLAY_WIDTH) + ((METRIC_HOG_X - TEXT_METRIC_HOG_WIDTH) + k)] =
                    (y << 0) + (uv << 8);
        }
    }

    /* Display "PSC" */
    for (j = 0; j < TEXT_DIGIT_HEIGHT; ++j)
    {
        for (k = 0; k < TEXT_METRIC_HOG_WIDTH; ++k)
        {
            value = text[(j * TEXT_METRIC_WIDTH) + (k + TEXT_METRIC_PSC_OFFSET)];

            y = ((TEXT_COLOR_Y * value) + (METRIC_COLOR_Y * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            u = ((TEXT_COLOR_U * value) + (METRIC_COLOR_U * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            v = ((TEXT_COLOR_V * value) + (METRIC_COLOR_V * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            uv = (((((METRIC_LOAD_RATE_Y + j) * DISPLAY_WIDTH) + ((METRIC_PSC_X - TEXT_METRIC_PSC_WIDTH) + k)) % 2)
                    == 0) ? u : v;

            buffer[((METRIC_LOAD_RATE_Y + j) * DISPLAY_WIDTH) + ((METRIC_PSC_X - TEXT_METRIC_PSC_WIDTH) + k)] =
                    (y << 0) + (uv << 8);
        }
    }

    /* Display "DOF" */
    for (j = 0; j < TEXT_DIGIT_HEIGHT; ++j)
    {
        for (k = 0; k < TEXT_METRIC_DOF_WIDTH; ++k)
        {
            value = text[(j * TEXT_METRIC_WIDTH) + (k + TEXT_METRIC_DOF_OFFSET)];

            y = ((TEXT_COLOR_Y * value) + (METRIC_COLOR_Y * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            u = ((TEXT_COLOR_U * value) + (METRIC_COLOR_U * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            v = ((TEXT_COLOR_V * value) + (METRIC_COLOR_V * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            uv = (((((METRIC_LOAD_RATE_Y + j) * DISPLAY_WIDTH) + ((METRIC_DOF_X - TEXT_METRIC_DOF_WIDTH) + k)) % 2)
                    == 0) ? u : v;

            buffer[((METRIC_LOAD_RATE_Y + j) * DISPLAY_WIDTH) + ((METRIC_DOF_X - TEXT_METRIC_DOF_WIDTH) + k)] =
                    (y << 0) + (uv << 8);
        }
    }

    /* Display "CPU" */
    for (j = 0; j < TEXT_DIGIT_HEIGHT; ++j)
    {
        for (k = 0; k < TEXT_METRIC_CPU_WIDTH; ++k)
        {
            value = text[(j * TEXT_METRIC_WIDTH) + (k + TEXT_METRIC_CPU_OFFSET)];

            y = ((TEXT_COLOR_Y * value) + (METRIC_COLOR_Y * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            u = ((TEXT_COLOR_U * value) + (METRIC_COLOR_U * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            v = ((TEXT_COLOR_V * value) + (METRIC_COLOR_V * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            uv = (((((METRIC_LOAD_RATE_Y + j) * DISPLAY_WIDTH) + ((METRIC_CPU_X - TEXT_METRIC_CPU_WIDTH) + k)) % 2)
                    == 0) ? u : v;

            buffer[((METRIC_LOAD_RATE_Y + j) * DISPLAY_WIDTH) + ((METRIC_CPU_X - TEXT_METRIC_CPU_WIDTH) + k)] =
                    (y << 0) + (uv << 8);
        }
    }

    return 0;
}

/***************************************************************************
*   Function: display_title_text
*
*   Description:
*       Display title
*
*   Parameters:
*       buffer - output buffer
*
*   Return:
*       Success or not
*/
static uint32_t display_title_text(uint16_t *buffer)
{
    uint8_t y;
    uint8_t uv;
    uint8_t u;
    uint8_t v;
    uint8_t value;
    uint32_t j;
    uint32_t k;
    uint32_t start_x = 0;
    uint32_t start_y = 0;

    /* Display "ISP output frames" */
    start_x = TITLE_RECT_START_X + (TITLE_RECT_WIDTH - TEXT_TITLE_ISP_WIDTH) / 2;
    start_y = TITLE_RECT_START_ISP_Y + (TITLE_RECT_HEIGHT - TEXT_TITLE_ISP_HEIGHT) / 2;

    for (j = 0; j < TEXT_TITLE_ISP_HEIGHT; ++j)
    {
        for (k = 0; k < TEXT_TITLE_ISP_WIDTH; ++k)
        {
            value = title[(j * TEXT_TITLE_ISP_WIDTH) + (k + TEXT_TITLE_ISP_OFFSET)];

            y = ((TEXT_TITLE_COLOR_Y * value) + (METRIC_COLOR_Y * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            u = ((TEXT_TITLE_COLOR_U * value) + (METRIC_COLOR_U * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            v = ((TEXT_TITLE_COLOR_V * value) + (METRIC_COLOR_V * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            uv = (((((start_y + j) * DISPLAY_WIDTH) + (start_x + k)) % 2) == 0) ? u : v;

            buffer[((start_y + j) * DISPLAY_WIDTH) + (start_x + k)] = (y << 0) + (uv << 8);
        }
    }

    /* Display "DOF motion vectors on rectified frames" */
    start_x = TITLE_RECT_START_X + (TITLE_RECT_WIDTH - TEXT_TITLE_DOF_WIDTH) / 2;
    start_y = TITLE_RECT_START_DOF_Y + (TITLE_RECT_HEIGHT - TEXT_TITLE_DOF_HEIGHT) / 2;

    for (j = 0; j < TEXT_TITLE_DOF_HEIGHT; ++j)
    {
        for (k = 0; k < TEXT_TITLE_DOF_WIDTH; ++k)
        {
            value = title[(j * TEXT_TITLE_DOF_WIDTH) + (k + TEXT_TITLE_DOF_OFFSET)];

            y = ((TEXT_TITLE_COLOR_Y * value) + (METRIC_COLOR_Y * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            u = ((TEXT_TITLE_COLOR_U * value) + (METRIC_COLOR_U * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            v = ((TEXT_TITLE_COLOR_V * value) + (METRIC_COLOR_V * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            uv = (((((start_y + j) * DISPLAY_WIDTH) + (start_x + k)) % 2) == 0) ? u : v;

            buffer[((start_y + j) * DISPLAY_WIDTH) + (start_x + k)] = (y << 0) + (uv << 8);
        }
    }

    return 0;
}

/***************************************************************************
*   Function: display_subtitle_text
*
*   Description:
*       Display title
*
*   Parameters:
*       buffer - output buffer
*
*   Return:
*       Success or not
*/
static uint32_t display_subtitle_text(uint16_t *buffer)
{
    uint8_t y;
    uint8_t uv;
    uint8_t u;
    uint8_t v;
    uint8_t value;
    uint32_t j;
    uint32_t k;
    uint32_t start_x = 0;
    uint32_t start_y = 0;

    /* Display sub titles for ISP output */
    start_x = STITLE_RECT_ISP_0_X + ((STITLE_RECT_WIDTH - TEXT_STITLE_ISP_WIDTH) / 2);
    start_y = STITLE_RECT_ISP_Y + ((STITLE_RECT_HEIGHT - TEXT_STITLE_ISP_HEIGHT) / 2);

    for (j = 0; j < TEXT_STITLE_ISP_HEIGHT; ++j)
    {
        for (k = 0; k < TEXT_STITLE_ISP_WIDTH; ++k)
        {
            value = sub_title[(j * TEXT_STITLE_WIDTH) + (k + TEXT_STITLE_ISP_OFFSET)];

            y = ((TEXT_COLOR_Y * value) + (METRIC_COLOR_Y * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            u = ((TEXT_COLOR_U * value) + (METRIC_COLOR_U * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            v = ((TEXT_COLOR_V * value) + (METRIC_COLOR_V * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            uv = (((((start_y + j) * DISPLAY_WIDTH) + (start_x + k)) % 2) == 0) ? u : v;

            buffer[((start_y + j) * DISPLAY_WIDTH) + (start_x + k)] = (y << 0) + (uv << 8);
        }
    }

    start_x = STITLE_RECT_ISP_1_X + ((STITLE_RECT_WIDTH - TEXT_STITLE_ISP_WIDTH) / 2);
    start_y = STITLE_RECT_ISP_Y + ((STITLE_RECT_HEIGHT - TEXT_STITLE_ISP_HEIGHT) / 2);

    for (j = 0; j < TEXT_STITLE_ISP_HEIGHT; ++j)
    {
        for (k = 0; k < TEXT_STITLE_ISP_WIDTH; ++k)
        {
            value = sub_title[(j * TEXT_STITLE_WIDTH) + (k + TEXT_STITLE_ISP_OFFSET)];

            y = ((TEXT_COLOR_Y * value) + (METRIC_COLOR_Y * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            u = ((TEXT_COLOR_U * value) + (METRIC_COLOR_U * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            v = ((TEXT_COLOR_V * value) + (METRIC_COLOR_V * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            uv = (((((start_y + j) * DISPLAY_WIDTH) + (start_x + k)) % 2) == 0) ? u : v;

            buffer[((start_y + j) * DISPLAY_WIDTH) + (start_x + k)] = (y << 0) + (uv << 8);
        }
    }

    start_x = STITLE_RECT_ISP_2_X + ((STITLE_RECT_WIDTH - TEXT_STITLE_ISP_WIDTH) / 2);
    start_y = STITLE_RECT_ISP_Y + ((STITLE_RECT_HEIGHT - TEXT_STITLE_ISP_HEIGHT) / 2);

    for (j = 0; j < TEXT_STITLE_ISP_HEIGHT; ++j)
    {
        for (k = 0; k < TEXT_STITLE_ISP_WIDTH; ++k)
        {
            value = sub_title[(j * TEXT_STITLE_WIDTH) + (k + TEXT_STITLE_ISP_OFFSET)];

            y = ((TEXT_COLOR_Y * value) + (METRIC_COLOR_Y * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            u = ((TEXT_COLOR_U * value) + (METRIC_COLOR_U * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            v = ((TEXT_COLOR_V * value) + (METRIC_COLOR_V * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            uv = (((((start_y + j) * DISPLAY_WIDTH) + (start_x + k)) % 2) == 0) ? u : v;

            buffer[((start_y + j) * DISPLAY_WIDTH) + (start_x + k)] = (y << 0) + (uv << 8);
        }
    }

    start_x = STITLE_RECT_ISP_3_X + ((STITLE_RECT_WIDTH - TEXT_STITLE_ISP_WIDTH) / 2);
    start_y = STITLE_RECT_ISP_Y + ((STITLE_RECT_HEIGHT - TEXT_STITLE_ISP_HEIGHT) / 2);

    for (j = 0; j < TEXT_STITLE_ISP_HEIGHT; ++j)
    {
        for (k = 0; k < TEXT_STITLE_ISP_WIDTH; ++k)
        {
            value = sub_title[(j * TEXT_STITLE_WIDTH) + (k + TEXT_STITLE_ISP_OFFSET)];

            y = ((TEXT_COLOR_Y * value) + (METRIC_COLOR_Y * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            u = ((TEXT_COLOR_U * value) + (METRIC_COLOR_U * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            v = ((TEXT_COLOR_V * value) + (METRIC_COLOR_V * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            uv = (((((start_y + j) * DISPLAY_WIDTH) + (start_x + k)) % 2) == 0) ? u : v;

            buffer[((start_y + j) * DISPLAY_WIDTH) + (start_x + k)] = (y << 0) + (uv << 8);
        }
    }

    /* Display sub titles for DOF output */
    start_x = STITLE_RECT_DOF_0_X + ((STITLE_RECT_WIDTH - TEXT_STITLE_DOF_WIDTH) / 2);
    start_y = STITLE_RECT_DOF_Y + ((STITLE_RECT_HEIGHT - TEXT_STITLE_DOF_HEIGHT) / 2);

    for (j = 0; j < TEXT_STITLE_DOF_HEIGHT; ++j)
    {
        for (k = 0; k < TEXT_STITLE_DOF_WIDTH; ++k)
        {
            value = sub_title[(j * TEXT_STITLE_WIDTH) + (k + TEXT_STITLE_DOF_OFFSET)];

            y = ((TEXT_COLOR_Y * value) + (METRIC_COLOR_Y * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            u = ((TEXT_COLOR_U * value) + (METRIC_COLOR_U * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            v = ((TEXT_COLOR_V * value) + (METRIC_COLOR_V * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            uv = (((((start_y + j) * DISPLAY_WIDTH) + (start_x + k)) % 2) == 0) ? u : v;

            buffer[((start_y + j) * DISPLAY_WIDTH) + (start_x + k)] = (y << 0) + (uv << 8);
        }
    }

    start_x = STITLE_RECT_DOF_1_X + ((STITLE_RECT_WIDTH - TEXT_STITLE_DOF_WIDTH) / 2);
    start_y = STITLE_RECT_DOF_Y + ((STITLE_RECT_HEIGHT - TEXT_STITLE_DOF_HEIGHT) / 2);

    for (j = 0; j < TEXT_STITLE_DOF_HEIGHT; ++j)
    {
        for (k = 0; k < TEXT_STITLE_DOF_WIDTH; ++k)
        {
            value = sub_title[(j * TEXT_STITLE_WIDTH) + (k + TEXT_STITLE_DOF_OFFSET)];

            y = ((TEXT_COLOR_Y * value) + (METRIC_COLOR_Y * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            u = ((TEXT_COLOR_U * value) + (METRIC_COLOR_U * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            v = ((TEXT_COLOR_V * value) + (METRIC_COLOR_V * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            uv = (((((start_y + j) * DISPLAY_WIDTH) + (start_x + k)) % 2) == 0) ? u : v;

            buffer[((start_y + j) * DISPLAY_WIDTH) + (start_x + k)] = (y << 0) + (uv << 8);
        }
    }

    start_x = STITLE_RECT_DOF_2_X + ((STITLE_RECT_WIDTH - TEXT_STITLE_DOF_WIDTH) / 2);
    start_y = STITLE_RECT_DOF_Y + ((STITLE_RECT_HEIGHT - TEXT_STITLE_DOF_HEIGHT) / 2);

    for (j = 0; j < TEXT_STITLE_DOF_HEIGHT; ++j)
    {
        for (k = 0; k < TEXT_STITLE_DOF_WIDTH; ++k)
        {
            value = sub_title[(j * TEXT_STITLE_WIDTH) + (k + TEXT_STITLE_DOF_OFFSET)];

            y = ((TEXT_COLOR_Y * value) + (METRIC_COLOR_Y * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            u = ((TEXT_COLOR_U * value) + (METRIC_COLOR_U * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            v = ((TEXT_COLOR_V * value) + (METRIC_COLOR_V * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            uv = (((((start_y + j) * DISPLAY_WIDTH) + (start_x + k)) % 2) == 0) ? u : v;

            buffer[((start_y + j) * DISPLAY_WIDTH) + (start_x + k)] = (y << 0) + (uv << 8);
        }
    }

    start_x = STITLE_RECT_DOF_3_X + ((STITLE_RECT_WIDTH - TEXT_STITLE_DOF_WIDTH) / 2);
    start_y = STITLE_RECT_DOF_Y + ((STITLE_RECT_HEIGHT - TEXT_STITLE_DOF_HEIGHT) / 2);

    for (j = 0; j < TEXT_STITLE_DOF_HEIGHT; ++j)
    {
        for (k = 0; k < TEXT_STITLE_DOF_WIDTH; ++k)
        {
            value = sub_title[(j * TEXT_STITLE_WIDTH) + (k + TEXT_STITLE_DOF_OFFSET)];

            y = ((TEXT_COLOR_Y * value) + (METRIC_COLOR_Y * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            u = ((TEXT_COLOR_U * value) + (METRIC_COLOR_U * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            v = ((TEXT_COLOR_V * value) + (METRIC_COLOR_V * (TEXT_MAX_VALUE - value))) / TEXT_MAX_VALUE;
            uv = (((((start_y + j) * DISPLAY_WIDTH) + (start_x + k)) % 2) == 0) ? u : v;

            buffer[((start_y + j) * DISPLAY_WIDTH) + (start_x + k)] = (y << 0) + (uv << 8);
        }
    }

    return 0;
}

/***************************************************************************
*   Function: display_rectangle
*
*   Description:
*       Display rectangle
*
*   Parameters:
*       buffer - output buffer
*       start_x - start position x
*       start_y - start position y
*       width - width of the rectangle
*       height - height of the rectangle
*       corner - radius of the circle
*
*   Return:
*       Success or not
*/
static uint32_t display_rectangle(uint16_t *buffer, uint32_t start_x, uint32_t start_y, uint32_t width, uint32_t height,
        uint32_t corner)
{
    uint8_t y;
    uint8_t uv;
    uint32_t i;
    uint32_t j;

    for (i = start_y; i < (start_y + height); ++i)
    {
        for (j = start_x; j < (start_x + width); ++j)
        {
            y = METRIC_COLOR_Y;
            uv = ((j % 2) == 0) ? METRIC_COLOR_U : METRIC_COLOR_V;

            /* Left top corner */
            if ((i < (start_y + corner)) && (j < (start_x + corner)))
            {
                float temp = pow (((start_y + corner) - i), 2)
                        + pow (((start_x + corner) - j), 2);

                if (temp < pow (corner, 2))
                    buffer[(i * DISPLAY_WIDTH) + j] = (y << 0) + (uv << 8);
            }
            /* Left bottom corner */
            else if ((i > ((start_y + height) - corner))
                    && (j < (start_x + corner)))
            {
                float temp = pow (i - ((start_y + height) - corner), 2)
                        + pow (((start_x + corner) - j), 2);

                if (temp < pow (corner, 2))
                    buffer[(i * DISPLAY_WIDTH) + j] = (y << 0) + (uv << 8);
            }
            /* Right top corner */
            else if ((i < (start_y + corner))
                    && (j > ((start_x + width) - corner)))
            {
                float temp = pow (((start_y + corner) - i), 2)
                        + pow (j - ((start_x + width) - corner), 2);

                if (temp < pow (corner, 2))
                    buffer[(i * DISPLAY_WIDTH) + j] = (y << 0) + (uv << 8);
            }
            /* Right bottom corner */
            else if ((i > ((start_y + height) - corner))
                    && (j > ((start_x + width) - corner)))
            {
                float temp = pow (i - ((start_y + height) - corner), 2)
                        + pow (j - ((start_x + width) - corner), 2);

                if (temp < pow (corner, 2))
                    buffer[(i * DISPLAY_WIDTH) + j] = (y << 0) + (uv << 8);
            }
            else
            {
                buffer[(i * DISPLAY_WIDTH) + j] = (y << 0) + (uv << 8);
            }
        }
    }

    return 0;
}

/***************************************************************************
*   Function: display_logo
*
*   Description:
*       Display logo
*
*   Parameters:
*       buffer - output buffer
*
*   Return:
*       Success or not
*/
static uint32_t display_logo(uint16_t *buffer)
{
    uint8_t y;
    uint8_t y_background;
    uint8_t uv;
    uint8_t u;
    uint8_t v;
    uint8_t uv_background;
    uint8_t value;
    uint32_t j;
    uint32_t k;

    /* Display LCX */
    for (j = 0; j < LOGO_LCX_HEIGHT; ++j)
    {
        for (k = 0; k < LOGO_WIDTH; ++k)
        {
            value = logo[(j * LOGO_WIDTH) + (k + LOGO_LCX_OFFSET)];

            y_background = buffer[((LOGO_LCX_Y + j) * DISPLAY_WIDTH) + (LOGO_LCX_X + k)] & 0xFF;
            uv_background = buffer[((LOGO_LCX_Y + j) * DISPLAY_WIDTH) + (LOGO_LCX_X + k)] >> 8;

            y = ((LOGO_COLOR_LCX_Y * value) + (y_background * (LOGO_LCX_MAX - value))) / LOGO_LCX_MAX;
            u = ((LOGO_COLOR_LCX_U * value) + (uv_background * (LOGO_LCX_MAX - value))) / LOGO_LCX_MAX;
            v = ((LOGO_COLOR_LCX_V * value) + (uv_background * (LOGO_LCX_MAX - value))) / LOGO_LCX_MAX;
            uv = (((((LOGO_LCX_Y + j) * DISPLAY_WIDTH) + (LOGO_LCX_X + k)) % 2) == 0) ? u : v;

            buffer[((LOGO_LCX_Y + j) * DISPLAY_WIDTH) + (LOGO_LCX_X + k)] = (y << 0) + (uv << 8);
        }
    }

    /* Display REN */
    for (j = 0; j < LOGO_REN_HEIGHT; ++j)
    {
        for (k = 0; k < LOGO_WIDTH; ++k)
        {
            value = logo[(j * LOGO_WIDTH) + (k + LOGO_REN_OFFSET)];

            y_background = buffer[((LOGO_REN_Y + j) * DISPLAY_WIDTH) + (LOGO_REN_X + k)] & 0xFF;
            uv_background = buffer[((LOGO_REN_Y + j) * DISPLAY_WIDTH) + (LOGO_REN_X + k)] >> 8;

            y = ((LOGO_COLOR_REN_Y * value) + (y_background * (LOGO_REN_MAX - value))) / LOGO_REN_MAX;
            u = ((LOGO_COLOR_REN_U * value) + (uv_background * (LOGO_REN_MAX - value))) / LOGO_REN_MAX;
            v = ((LOGO_COLOR_REN_V * value) + (uv_background * (LOGO_REN_MAX - value))) / LOGO_REN_MAX;
            uv = (((((LOGO_REN_Y + j) * DISPLAY_WIDTH) + (LOGO_REN_X + k)) % 2) == 0) ? u : v;

            buffer[((LOGO_REN_Y + j) * DISPLAY_WIDTH) + (LOGO_REN_X + k)] = (y << 0) + (uv << 8);
        }
    }

    return 0;
}

/***************************************************************************
*   Function: display_overlay_hsv
*
*   Description:
*       Add DOF overlay
*
*   Parameters:
*       image - input buffer
*       pout_flow_buffer_l2 - dof buffer level 2
*       pout_flow_buffer_l1 - dof buffer level 1
*       x - index x
*       y - index y
*       r, g, b -rgb component
*       avg - average of the vectors size
*
*   Return:
*       Success or not
*/
static void display_overlay_hsv(uint16_t *image, uint32_t *pout_flow_buffer_l2, uint32_t *pout_flow_buffer_l1,
        uint32_t x, uint32_t y, uint8_t *r, uint8_t *g, uint8_t *b, float avg)
{
    float red;
    float green;
    float blue;
    float valf_hori;
    float valf_vert;
    float angle;
    float vector_size;
    float hsv_h;
    float hsv_s;
    float hsv_v;

    int16_t val_hori;
    int16_t val_vert;
    uint32_t i = y * SUB_IMAGE_FACTOR;
    uint32_t j = x * SUB_IMAGE_FACTOR;
    uint32_t val32;
    int32_t index_dof_output;
    int32_t qual;
    uint32_t pix;

    /* Extract DOF output from the level L1 or L2 */
    if ((j < ((frame_resolution.frame_out_width_imr - (frame_resolution.frame_out_width_dof_l1 * 2)) / 2))
            || (j >= (((frame_resolution.frame_out_width_imr - (frame_resolution.frame_out_width_dof_l1 * 2)) / 2)
            + (frame_resolution.frame_out_width_dof_l1 * 2))))
    {
        index_dof_output = floor (i / 2) * frame_resolution.frame_out_width_dof_l2 + floor (j / 2);

        val32 = pout_flow_buffer_l2[index_dof_output];
    }
    else
    {
        index_dof_output = floor (i / 2) * frame_resolution.frame_out_width_dof_l1 + floor ((j
            - ((frame_resolution.frame_out_width_imr - (frame_resolution.frame_out_width_dof_l1 * 2)) / 2)) / 2);

        val32 = pout_flow_buffer_l1[index_dof_output];
    }

    /* Extract quality */
    qual = val32 & QUALITY_MASK;

    if ((0 != qual) & (i < (frame_resolution.frame_out_height_dof_l1 * 2)))
    {
        /* Extract motion components (offsets from DOF documentation) */
        val_hori = (int16_t) ((val32 & HORIZONTAL_MASK) >> 16);
        val_vert = (int16_t) ((val32 & VERTICAL_MASK) >> 6);

        valf_hori = (float) (abs (val_hori) >> 7);
        valf_vert = (float) (abs (val_vert) >> 7);

        /* Compute vector size */
        vector_size = sqrtf (powf (valf_hori, 2.0) + powf (valf_vert, 2.0));

        /* Divide vector size by the average of the vectors size of the image for a better visualization on screen */
        vector_size = vector_size / avg;
        if (vector_size >= 1)
            vector_size = 0.99;

        /* Compute vector direction */
        angle = atanf (valf_hori / valf_vert) * 180 / M_PI;

        if ((val_hori >= 0) && ((val_vert >= 0)))
        {
            angle = (90 - angle);
        }
        else if ((val_hori < 0) && ((val_vert >= 0)))
        {
            angle = angle + 90;
        }
        else if ((val_hori >= 0) && ((val_vert < 0)))
        {
            angle = angle + 270;
        }
        else if ((val_hori < 0) && ((val_vert < 0)))
        {
            angle = (90 - angle) + 180;
        }

        /* Adjust color angle to fit with Renesas view */
        angle = (float) (((uint32_t) angle + 90) % 360);

        /* Extract input pixel value */
        pix = (uint16_t) image[((i * frame_resolution.frame_out_stride_imr) + j)];

        /* Compute output value based on HSV and RGB colorspace */
        hsv_h = angle; /* Select color */
        hsv_s = vector_size; /* Select saturation */
        hsv_v = ((float) pix) / 256.0; /* Select value */

        /* Convert current pixel from HSV to RGB */
        display_hsv_to_rgb (&red, &green, &blue, hsv_h, hsv_s, hsv_v);

        *r = (uint8_t) (red * 255.0);
        *g = (uint8_t) (green * 255.0);
        *b = (uint8_t) (blue * 255.0);
    }
    else
    {
        /* Extract input pixel value */
        pix = (uint16_t) image[((i * frame_resolution.frame_out_stride_imr) + j)];

        *r = (uint8_t) (pix);
        *g = (uint8_t) (pix);
        *b = (uint8_t) (pix);
    }
}

/***************************************************************************
*   Function: display_find_max_vector_dof
*
*   Description:
*       Compute DOF vector size
*
*   Parameters:
*       pout_flow_buffer_l2 - dof buffer level 2
*       pout_flow_buffer_l1 - dof buffer level 1
*       x - index x
*       y - index y
*
*   Return:
*       Vector size
*/
static float display_compute_vector_dof(uint32_t *pout_flow_buffer_l2, uint32_t *pout_flow_buffer_l1, uint32_t x,
        uint32_t y)
{
    float valf_hori;
    float valf_vert;
    float vector_size = 0;

    int16_t val_hori;
    int16_t val_vert;
    int32_t qual;
    uint32_t i = y * SUB_IMAGE_FACTOR;
    uint32_t j = x * SUB_IMAGE_FACTOR;
    uint32_t val32;
    int32_t index_dof_output;

    /* Extract DOF output from the level L1 or L2 */
    if ((j < ((frame_resolution.frame_out_width_imr - (frame_resolution.frame_out_width_dof_l1 * 2)) / 2))
            || (j
                    >= (((frame_resolution.frame_out_width_imr - (frame_resolution.frame_out_width_dof_l1 * 2)) / 2)
                            + (frame_resolution.frame_out_width_dof_l1 * 2))))
    {
        index_dof_output = floor (i / 2) * frame_resolution.frame_out_width_dof_l2 + floor (j / 2);

        val32 = pout_flow_buffer_l2[index_dof_output];
    }
    else
    {
        index_dof_output = floor (i / 2) * frame_resolution.frame_out_width_dof_l1  + floor ((j
                - ((frame_resolution.frame_out_width_imr - (frame_resolution.frame_out_width_dof_l1 * 2)) / 2)) / 2);

        val32 = pout_flow_buffer_l1[index_dof_output];
    }

    /* Extract quality */
    qual = val32 & QUALITY_MASK;

    if ((0 != qual) & (i < (frame_resolution.frame_out_height_dof_l1 * 2)))
    {
        /* Extract motion components (offsets from DOF documentation) */
        val_hori = (int16_t) ((val32 & HORIZONTAL_MASK) >> 16);
        val_vert = (int16_t) ((val32 & VERTICAL_MASK) >> 6);

        valf_hori = (float) (abs (val_hori) >> 7);
        valf_vert = (float) (abs (val_vert) >> 7);

        /* Compute vector size */
        vector_size = sqrtf (powf (valf_hori, 2.0) + powf (valf_vert, 2.0));
    }

    return vector_size;
}

/***********************************************************************************************************************
 * Function Name: display_hsv_to_rgb
 * Description  : Converts color space from HSV to RGB.
 * Arguments    : rgb_red - Output red component
 *                rgb_green - Output green component
 *                rgb_blue - Output blue component
 *                hsv_hue - Input hue component [0 360]
 *                hsv_sat - Input sat component [0.0 1.0]
 *                hsv_val - Input value component [0.0 1.0]
 * Return Value : None
 ***********************************************************************************************************************/
static void display_hsv_to_rgb (float *rgb_red, float *rgb_green, float *rgb_blue, float hsv_hue, float hsv_sat,
        float hsv_val)
{
    float hsv_c = hsv_val * hsv_sat;
    float hsv_x = hsv_c * (1 - fabs (fmod (hsv_hue / 60, 2) - 1));
    float hsv_m = hsv_val - hsv_c;

    if ((0 <= hsv_hue) && (hsv_hue < 60))           /* Red to Yellow */
    {
        *rgb_red = hsv_c;
        *rgb_green = hsv_x;
        *rgb_blue = 0;
    }
    else if ((60 <= hsv_hue) && (hsv_hue < 120))    /* Yellow to Green */
    {
        *rgb_red = hsv_x;
        *rgb_green = hsv_c;
        *rgb_blue = 0;
    }
    else if ((120 <= hsv_hue) && (hsv_hue < 180))   /* Green to Blue */
    {
        *rgb_red = 0;
        *rgb_green = hsv_c;
        *rgb_blue = hsv_x;
    }
    else if ((180 <= hsv_hue) && (hsv_hue < 240))   /* Blue to Cyan */
    {
        *rgb_red = 0;
        *rgb_green = hsv_x;
        *rgb_blue = hsv_c;
    }
    else if ((240 <= hsv_hue) && (hsv_hue < 300))   /* Cyan to Magenta */
    {
        *rgb_red = hsv_x;
        *rgb_green = 0;
        *rgb_blue = hsv_c;
    }
    else if ((300 <= hsv_hue) && (hsv_hue < 360))   /* Magenta to Red */
    {
        *rgb_red = hsv_c;
        *rgb_green = 0;
        *rgb_blue = hsv_x;
    }
    else                                            /* White */
    {
        *rgb_red = hsv_c;
        *rgb_green = hsv_c;
        *rgb_blue = hsv_c;
    }

    (*rgb_red) += hsv_m;
    (*rgb_green) += hsv_m;
    (*rgb_blue) += hsv_m;
}
/***********************************************************************************************************************
 End of function display_hsv_to_rgb
 ***********************************************************************************************************************/

/** The functions, below, are provided by the modetest sample code of the DRM library and are not modified **/

static inline int64_t U642I64(uint64_t val)
{
    return (int64_t)*((int64_t *)&val);
}

#define bit_name_fn(res)                    \
const char * res##_str(int type) {              \
    unsigned int i;                     \
    const char *sep = "";                   \
    for (i = 0; i < ARRAY_SIZE(res##_names); i++) {     \
        if (type & (1 << i)) {              \
            printf("%s%s", sep, res##_names[i]);    \
            sep = ", ";             \
        }                       \
    }                           \
    return NULL;                        \
}

static void free_resources(struct resources *res)
{
    int i;

    if (!res)
        return;

#define free_resource(_res, type, Type)                 \
    do {                                    \
        if (!(_res)->type##s)                       \
            break;                          \
        for (i = 0; i < (int)(_res)->count_##type##s; ++i) {    \
            if (!(_res)->type##s[i].type)               \
                break;                      \
            drmModeFree##Type((_res)->type##s[i].type);     \
        }                               \
        free((_res)->type##s);                      \
    } while (0)

#define free_properties(_res, type)                 \
    do {                                    \
        for (i = 0; i < (int)(_res)->count_##type##s; ++i) {    \
            unsigned int j;                                     \
            for (j = 0; j < res->type##s[i].props->count_props; ++j)\
                drmModeFreeProperty(res->type##s[i].props_info[j]);\
            free(res->type##s[i].props_info);           \
            drmModeFreeObjectProperties(res->type##s[i].props); \
        }                               \
    } while (0)

    free_properties(res, plane);
    free_resource(res, plane, Plane);

    free_properties(res, connector);
    free_properties(res, crtc);

    for (i = 0; i < res->count_connectors; i++)
        free(res->connectors[i].name);

    free_resource(res, fb, FB);
    free_resource(res, connector, Connector);
    free_resource(res, encoder, Encoder);
    free_resource(res, crtc, Crtc);

    free(res);
}

static struct resources *get_resources(struct device *dev)
{
    drmModeRes *_res;
    drmModePlaneRes *plane_res;
    struct resources *res;
    int i;

    res = calloc(1, sizeof(*res));
    if (res == 0)
        return NULL;

    drmSetClientCap(dev->fd, DRM_CLIENT_CAP_UNIVERSAL_PLANES, 1);

    _res = drmModeGetResources(dev->fd);
    if (!_res) {
        fprintf(stderr, "drmModeGetResources failed: %s\n",
            strerror(errno));
        free(res);
        return NULL;
    }

    res->count_crtcs = _res->count_crtcs;
    res->count_encoders = _res->count_encoders;
    res->count_connectors = _res->count_connectors;
    res->count_fbs = _res->count_fbs;

    res->crtcs = calloc(res->count_crtcs, sizeof(*res->crtcs));
    res->encoders = calloc(res->count_encoders, sizeof(*res->encoders));
    res->connectors = calloc(res->count_connectors, sizeof(*res->connectors));
    res->fbs = calloc(res->count_fbs, sizeof(*res->fbs));

    if (!res->crtcs || !res->encoders || !res->connectors || !res->fbs) {
        drmModeFreeResources(_res);
        goto error;
    }

#define get_resource(_res, __res, type, Type)                   \
    do {                                    \
        for (i = 0; i < (int)(_res)->count_##type##s; ++i) {    \
            uint32_t type##id = (__res)->type##s[i];            \
            (_res)->type##s[i].type =                           \
                drmModeGet##Type(dev->fd, type##id);            \
            if (!(_res)->type##s[i].type)                       \
                fprintf(stderr, "could not get %s %i: %s\n",    \
                    #type, type##id,                            \
                    strerror(errno));           \
        }                               \
    } while (0)

    get_resource(res, _res, crtc, Crtc);
    get_resource(res, _res, encoder, Encoder);
    get_resource(res, _res, connector, Connector);
    get_resource(res, _res, fb, FB);

    drmModeFreeResources(_res);

    /* Set the name of all connectors based on the type name and the per-type ID. */
    for (i = 0; i < res->count_connectors; i++) {
        struct connector *connector = &res->connectors[i];
        drmModeConnector *conn = connector->connector;
        int num;

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wimplicit-function-declaration"
        num = asprintf(&connector->name, "%s-%u",
             util_lookup_connector_type_name(conn->connector_type),
             conn->connector_type_id);
#pragma GCC diagnostic pop

        if (num < 0)
            goto error;
    }

#define get_properties(_res, type, Type)                    \
    do {                                    \
        for (i = 0; i < (int)(_res)->count_##type##s; ++i) {    \
            struct type *obj = &res->type##s[i];            \
            unsigned int j;                     \
            obj->props =                        \
                drmModeObjectGetProperties(dev->fd, obj->type->type##_id, \
                               DRM_MODE_OBJECT_##Type); \
            if (!obj->props) {                  \
                fprintf(stderr,                 \
                    "could not get %s %i properties: %s\n", \
                    #type, obj->type->type##_id,        \
                    strerror(errno));           \
                continue;                   \
            }                           \
            obj->props_info = calloc(obj->props->count_props,   \
                         sizeof(*obj->props_info)); \
            if (!obj->props_info)                   \
                continue;                   \
            for (j = 0; j < obj->props->count_props; ++j)       \
                obj->props_info[j] =                \
                    drmModeGetProperty(dev->fd, obj->props->props[j]); \
        }                               \
    } while (0)

    get_properties(res, crtc, CRTC);
    get_properties(res, connector, CONNECTOR);

    for (i = 0; i < res->count_crtcs; ++i)
        res->crtcs[i].mode = &res->crtcs[i].crtc->mode;

    plane_res = drmModeGetPlaneResources(dev->fd);
    if (!plane_res) {
        fprintf(stderr, "drmModeGetPlaneResources failed: %s\n",
            strerror(errno));
        return res;
    }

    res->count_planes = plane_res->count_planes;

    res->planes = calloc(res->count_planes, sizeof(*res->planes));
    if (!res->planes) {
        drmModeFreePlaneResources(plane_res);
        goto error;
    }

    get_resource(res, plane_res, plane, Plane);
    drmModeFreePlaneResources(plane_res);
    get_properties(res, plane, PLANE);

    return res;

error:
    free_resources(res);
    return NULL;
}

static struct crtc *get_crtc_by_id(struct device *dev, uint32_t id)
{
    int i;

    for (i = 0; i < dev->resources->count_crtcs; ++i) {
        drmModeCrtc *crtc = dev->resources->crtcs[i].crtc;
        if (crtc && crtc->crtc_id == id)
            return &dev->resources->crtcs[i];
    }

    return NULL;
}

static uint32_t get_crtc_mask(struct device *dev, struct crtc *crtc)
{
    unsigned int i;

    for (i = 0; i < (unsigned int)dev->resources->count_crtcs; i++) {
        if (crtc->crtc->crtc_id == dev->resources->crtcs[i].crtc->crtc_id)
            return 1 << i;
    }
    /* Unreachable: crtc->crtc is one of resources->crtcs[] */
    /* Don't return zero or static analysers will complain */
    abort();
    return 0;
}

/* -----------------------------------------------------------------------------
 * Pipes and planes
 */

static bool format_support(const drmModePlanePtr ovr, uint32_t fmt)
{
    unsigned int i;

    for (i = 0; i < ovr->count_formats; ++i) {
        if (ovr->formats[i] == fmt)
            return true;
    }

    return false;
}

static int
bo_fb_create(int fd, unsigned int fourcc, const uint32_t w, const uint32_t h,
             enum util_fill_pattern pat, struct bo **out_bo, unsigned int *out_fb_id)
{
    uint32_t handles[4] = {0}, pitches[4] = {0}, offsets[4] = {0};
    struct bo *bo;
    unsigned int fb_id;

    bo = bo_create(fd, fourcc, w, h, handles, pitches, offsets, pat);

    if (bo == NULL)
        return -1;

    if (drmModeAddFB2(fd, w, h, fourcc, handles, pitches, offsets, &fb_id, 0)) {
        fprintf(stderr, "failed to add fb (%ux%u): %s\n", w, h, strerror(errno));
        bo_destroy(bo);
        return -1;
    }
    *out_bo = bo;
    *out_fb_id = fb_id;
    return 0;
}

static int set_plane(struct device *dev, struct plane_arg *p)
{
    drmModePlane *ovr;
    uint32_t plane_id;
    int crtc_x, crtc_y, crtc_w, crtc_h;
    struct crtc *crtc = NULL;
    unsigned int i, crtc_mask;

    /* Find an unused plane which can be connected to our CRTC. Find the
     * CRTC index first, then iterate over available planes.
     */
    crtc = get_crtc_by_id(dev, p->crtc_id);
    if (!crtc) {
        fprintf(stderr, "CRTC %u not found\n", p->crtc_id);
        return -1;
    }
    crtc_mask = get_crtc_mask(dev, crtc);
    plane_id = p->plane_id;

    for (i = 0; i < dev->resources->count_planes; i++) {
        ovr = dev->resources->planes[i].plane;
        if (!ovr)
            continue;

        if (plane_id && plane_id != ovr->plane_id)
            continue;

        if (!format_support(ovr, p->fourcc))
            continue;

        if ((ovr->possible_crtcs & crtc_mask) &&
            (ovr->crtc_id == 0 || ovr->crtc_id == p->crtc_id)) {
            plane_id = ovr->plane_id;
            break;
        }
    }

    if (i == dev->resources->count_planes) {
        fprintf(stderr, "no unused plane available for CRTC %u\n",
            p->crtc_id);
        return -1;
    }

    fprintf(stderr, "testing %dx%d@%s overlay plane %u\n",
        p->w, p->h, p->format_str, plane_id);

    /* just use single plane format for now.. */
    if (bo_fb_create(dev->fd, p->fourcc, p->w, p->h,
                     secondary_fill, &p->bo, &p->fb_id))
        return -1;

    crtc_w = p->w * p->scale;
    crtc_h = p->h * p->scale;
    if (!p->has_position) {
        /* Default to the middle of the screen */
        crtc_x = (crtc->mode->hdisplay - crtc_w) / 2;
        crtc_y = (crtc->mode->vdisplay - crtc_h) / 2;
    } else {
        crtc_x = p->x;
        crtc_y = p->y;
    }

    /* note src coords (last 4 args) are in Q16 format */
    if (drmModeSetPlane(dev->fd, plane_id, p->crtc_id, p->fb_id,
                0, crtc_x, crtc_y, crtc_w, crtc_h,
                0, 0, p->w << 16, p->h << 16)) {
        fprintf(stderr, "failed to enable plane: %s\n",
            strerror(errno));
        return -1;
    }

    ovr->crtc_id = p->crtc_id;

    return 0;
}

static void clear_planes(struct device *dev, struct plane_arg *p, unsigned int count)
{
    unsigned int i;

    for (i = 0; i < count; i++) {
        if (p[i].fb_id)
            drmModeRmFB(dev->fd, p[i].fb_id);
        if (p[i].bo)
            bo_destroy(p[i].bo);
    }
}

static void set_planes(struct device *dev, struct plane_arg *p, unsigned int count)
{
    unsigned int i;

    /* set up planes/overlays */
    for (i = 0; i < count; i++)
        if (set_plane(dev, &p[i]))
            return;
}
/*************************************** Copyright (c) 2021 LaCroix *****END OF FILE*******************************/
