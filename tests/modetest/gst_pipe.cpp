/**
* Copyright (c) 2021-2022 Hailo Technologies Ltd. All rights reserved.
* Distributed under the LGPL license (https://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt)
**/
// General cpp includes
#include <chrono>
#include <gst/gst.h>
#include <gst/video/video.h>
#include <gst/app/gstappsink.h>
#include <iostream>
#include <chrono>
#include <ctime>
#include <stdio.h>
#include <unistd.h>


int counter = 0;


//******************************************************************
// PIPELINE UTILITIES
//******************************************************************


static GstFlowReturn appsink_cb(GstAppSink * sink, gpointer udata)
{
    counter++;

    GstSample *sample;  // The new sample available in the appsink
    GstBuffer *buffer;  // The buffer stored in that sample
    GstMapInfo map;

    // Get the sample from the appsink
    sample = gst_app_sink_pull_sample(sink);
    
    // If no sample was available (premature  signal) then return, we will get it on the next pass
    if (nullptr == sample)
        return GST_FLOW_OK;

    buffer = gst_sample_get_buffer(sample);    // Extract the buffer from the sample
    // buffer = gst_buffer_make_writable(buffer); // Get a read/writeable copy
    gst_buffer_map(buffer, &map, GST_MAP_READ);

    uint16_t * data = (uint16_t *)map.data;

    std::cout << "Im in the appsink_cb - " << counter << std::endl;
    test_start(data);

    gst_buffer_unmap(buffer, &map);
    gst_buffer_unref(buffer);
    gst_sample_unref(sample);

    return GST_FLOW_OK;
}

void set_probe_callbacks(GstElement *pipeline)
{
    std::cout << "setting probes" << std::endl;
    GstElement *app_sink_element = gst_bin_get_by_name(GST_BIN(pipeline), "test_app_sink");
    g_signal_connect(app_sink_element, "new-sample", G_CALLBACK(appsink_cb), NULL);
}

/**
 * @brief Handle ending messages from the pipeline (EOS, errors, etc...)
 * 
 * @param bus  -  GstBus*
 *        The GstBus to handle.
 *
 * @param msg  -  GstMessage*
 *        The GstMessage to handle.
 */
static gboolean my_bus_callback (GstBus * bus, GstMessage * message, gpointer udata)
{
  GMainLoop *loop = (GMainLoop *)udata;

  switch (GST_MESSAGE_TYPE (message)) {
    case GST_MESSAGE_ERROR:{
      GError *err;
      gchar *debug;

      gst_message_parse_error (message, &err, &debug);
      g_print ("Error: %s\n", err->message);
      if (std::string(err->message) == "Output window was closed")
      {
        std::cout << "------------ WINDOW CLOSED --------------------" << std::endl;
      }
      g_error_free (err);
      g_free (debug);

      g_main_loop_quit (loop);
    //   pipe->stop();
      break;
    }
    case GST_MESSAGE_EOS:
      /* end-of-stream */
      g_print ("End of stream\n");
      g_main_loop_quit (loop);
      break;
    default:
      /* unhandled message */
      break;
  }

  /* we want to be notified again the next time there is a message
   * on the bus, so returning TRUE (FALSE means we want to stop watching
   * for messages on the bus and our callback should not be called again)
   */
  return TRUE;
}

int main(int argc, char *argv[])
{
    // Prepare pipeline components
    GstBus *bus;
    GMainLoop *loop;
    gst_init(&argc, &argv);  // Initialize Gstreamer
    loop = g_main_loop_new (NULL, FALSE);

    // Create the pipeline
    GstElement *pipeline = gst_parse_launch("filesrc location=/local/workspace/tappas/apps/gstreamer/resources/mp4/river_tiber.mp4 ! decodebin ! videoconvert ! appsink sync=false qos=false emit-signals=true name=test_app_sink", NULL);
    
    bus = gst_element_get_bus(pipeline);
    gst_bus_add_watch (bus, my_bus_callback, loop);
    set_probe_callbacks(pipeline);
    gst_element_set_state(pipeline, GST_STATE_PLAYING);
    g_main_loop_run(loop);

    // Free resources
    gst_element_set_state(pipeline, GST_STATE_NULL);
    gst_deinit();
    gst_object_unref(pipeline);
    gst_object_unref(bus);
    g_main_loop_unref (loop);

    return 0;
}